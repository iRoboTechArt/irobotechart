Title: Robotic
Date: 2018-04-25 22:30
Tags: jeux vidéos, robot
Category: iRoboTechArt
Authors: Victor Lohézic
Summary: RoboTiC est une plateforme pour enrichir sa culture robotique et technologique.
Image: images/projets/robotique/robotic/definir.png
Type: iRoboTechArt
Status: published

<script type="text/javascript">
  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems, options);
  });

  // Or with jQuery

  $(document).ready(function(){
    $('.collapsible').collapsible();
  });
</script>

<h2>RoboTiC</h2>
<br>
<center>
    <img class="responsive-img" src="images/projets/robotique/robotic/robotic.jpg" alt="image d'accueil" width="50%">
</center>
<br>
<ul class="collapsible">
  <li>
    <div class="collapsible-header"><i class="material-icons">notes</i>Présentation</div>
      <div class="collapsible-body"><span>RoboTiC est une plateforme qui vise à parfaire ses connaissances **technologiques** et **robotiques**. Elle repose sur trois rubriques qui sont : **définir**, **apprendre**, **se tester**.
      </span></div>
  </li>
  <li>
    <div class="collapsible-header"><i class="material-icons">link</i>Lien</div>
    <div class="collapsible-body"><span>
          [RoboTiC](https://robotic.irobotechart.com/)
       </span></div>
  </li>
  <li>
    <div class="collapsible-header"><i class="material-icons">update</i>Prochaines mises à jours</div>
    <div class="collapsible-body"><span>
    Ajout de la rubrique <b>Apprendre</b>
    <br>Ajout de la rubrique <b>Se Tester</b>
    </span></div>
  </li>
</ul>

<br>
