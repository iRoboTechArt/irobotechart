Title: Bomby
Date: 2018-03-27 22:30
Tags: jeux vidéos, robot
Category: iMakeGameDesign
Authors: Victor Lohézic
Summary: Le but est d'aider Bomby à échapper à BomBoss en le faisant sauter sur des plateformes.
Image: images/projets/jeux videos/bomby/bomby_logo.png
Type: iMakeGame
Status: published

<script type="text/javascript">
  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems, options);
  });

  // Or with jQuery

  $(document).ready(function(){
    $('.collapsible').collapsible();
  });
</script>

<h2>Bomby</h2>
<br>
<center>
    <img class="responsive-img" src="images/jeux_videos/bomby_g-develop/bomby.gif" alt="Animation de notre personnage" width="50%">
</center>
<br>
<ul class="collapsible">
  <li>
    <div class="collapsible-header"><i class="material-icons">notes</i>Présentation</div>
      <div class="collapsible-body"><span>Le but est d'aider Bomby à échapper à BomBoss en le faisant sauter sur des plateformes.
      <br>
      <div class="video-container">
        <iframe width="1194" height="672" src="https://www.youtube.com/embed/dJ8-yMwCjMg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
      </span></div>
  </li>
  <li>
    <div class="collapsible-header"><i class="material-icons">videogame_asset</i>Comment jouer ?</div>
    <div class="collapsible-body"><span>
       <table class="responsive-table">
      <thead>
        <tr>
            <th>Touches</th>
            <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Clic gauche avec la souris</td>
          <td>Bomby saute</td>
        </tr>
      </tbody>
    </table>
        </span></div>
  </li>
  <li>
    <div class="collapsible-header"><i class="material-icons">get_app</i>Jouer</div>
    <div class="collapsible-body"><span>
       <table class="responsive-table">
      <thead>
        <tr>
            <th>[Jouer en Ligne](https://bomby.irobotechart.com/)</th>
            <th>[Télécharger le jeu pour Windows](https://stockage.irobotechart.com/wl/?id=QD3LRA1EF2nH2e1Z0EEjQb6w9tIDhHhb)</th>
        </tr>
      </thead>
    </table>
        </span></div>
  </li>
  <li>
    <div class="collapsible-header"><i class="material-icons">info</i>Infos et Crédits</div>
    <div class="collapsible-body"><span>Bomby est un jeu développée avec le logiciel [Gdevelop](https://www.irobotechart.com/gdevelop-un-moteur-de-jeux.html). Le logiciel [Pixel](https://www.irobotechart.com/piskel-un-editeur-de-sprites.html) a été utilisé pour dessiner Bomby. Voici quelques informations sur les musiques du jeu :
    <br>
    <br>
    <div class="video-container">
      <iframe width="1194" height="672" src="https://www.youtube.com/embed/n8X9_MgEdCg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <center>[TheFatRat - Unity](https://www.youtube.com/watch?v=n8X9_MgEdCg)</center></span></div>
  </li>
</ul>

<br>
