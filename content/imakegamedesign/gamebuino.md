Title: La Gamebuino, une GameBoy Arduino
Date: 2018-01-17 12:42
Tags: gamebuino, arduino, jeux vidéos, consoles
Category: iMakeGameDesign
Authors: Victor Lohézic
Summary: Alors que l’on voit de nombreuses consoles de rétrogaming réalisées avec des Raspberry Pi. Je vais vous présenter l’original Gamebuino.
Image: images/imakegamedesign/gamebuino/thumbnail_banane.jpg
Type: Article
Status: published

Alors que l’on voit de nombreuses consoles de rétrogaming réalisées avec des Raspberry Pi. Je vais vous présenter l’original Gamebuino.
<center>
    <img class="responsive-img" src="images/imakegamedesign/gamebuino/gamebuino-meta-differents-angles.png" alt="Gamebuino meta sous différents angles">
</center>
La Gamebuino est une console rétro. Elle permet de créer ses propres jeux et d’apprendre la programmation. Elle est constituée d’une carte avec un microcontrôleur. Ce n’est pas un Arduino Uno ni Zero mais cela s’en rapproche. Elle a été modifiée pour ses besoins. Aurélien est à l’origine de ce projet. Il y a quatres ans lorsqu’il était encore étudiant. Mais il n’est pas tout seul à y travailler.
<center>
    <img class="responsive-img" src="images/imakegamedesign/gamebuino/etudiant.jpg" alt="Chambre d'étudiant">
</center>
Il est accompagné d’une vingtaine de personnes dont vous pouvez retrouver les noms sur leur page kickstarter. La Gamebuino est fabriquée et conçue en France. Aurélien, lorsqu’il était étudiant voulait partager sa passion. Ainsi lui est venu l’idée de créer cette console. Le temps passe, il fait des rencontres. Il réussit même à lancer sa startup. En 2014 est lancée la Gamebuino Classic. Plus tard, elle laisse place à une campagne kickstarter pour la GameBuino Meta. Les deux versions "Classique" et "Meta" sont aux prix respectifs (sans promotion) de 58,80 euros et 118,80 euros.
<center>
    <img class="responsive-img" src="images/imakegamedesign/gamebuino/banane.jpg" alt="Gamebuino meta à coté d'une banane">
</center>
Avec cette console, lui et son équipe veulent partager une expérience agréable et accessible, et ce dans de nombreux domaines, que ce soit la programmation, l’électronique ou encore l’impression 3D.

<div class="video-container">
    <iframe width="640" height="360" src="https://www.youtube.com/embed/uLS5WzyEINA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
<br>

En effet, le principal atout de cette console est sa pluridisciplinarité. Lors de la création de jeux, on va apprendre le langage C/C++ ou se perfectionner. Elle est adaptée à tout niveau car il est possible de coder son propre pong ou des jeux plus complexes.
<center>
    <img class="responsive-img" src="images/imakegamedesign/gamebuino/jeux.gif" alt="Différents jeux">
</center>
De plus, elle est open source. Il est donc possible d’ajouter des composants en plus. On pourra ainsi ajouter un gyroscope pour un jeu de course ou un capteur de luminosité afin de rendre l'expérience de jeu plus immersive. Dans la Gamebuino Meta, des LED RGB ajoutent des effets plutôt sympathiques.

<center>
    <img class="responsive-img" src="images/imakegamedesign/gamebuino/led.gif" alt="Gamebuino meta éclairée avec différentes couleurs">
</center>

Il est aussi possible de personnaliser sa console. On peut modéliser des pièces en 3D pour ensuite les imprimer. On peut s’approprier la Gamebuino.
Cette console a aussi l’avantage d’être portable. Ses petites dimensions lui permettent d’être transportée dans sa poche. Elle ajoute un aspect concret au jeu programmé. Contrairement à un jeu émulé sur un ordinateur, il est possible d’y jouer à tout moment. De ce point de vue, on peut la comparer à une GameBoy.

<center>
    <img class="responsive-img" src="images/imakegamedesign/gamebuino/tout-moment.png" alt="Image du créateur en train de jouer à sa gamebuino dans des toilettes">
</center>



De plus, le projet a un site gamebuino.com. Sur celui-ci se trouve de nombreuses ressources. Il y a une boutique pour acheter les différents modèles de la Gamebuino. Mais un blog est aussi présent. Le site est en encore en développement. Il semble travailler sur des tutoriels. En attendant, une page wiki est disponible pour mieux appréhender la Gamebuino. Le principal atout de ce site est le forum. En effet, le projet dispose d’une communauté très présente. En cas de problème, cela permet de pouvoir compter sur des personnes qui vous viendront en aide.

<center>
    <img class="responsive-img" src="images/imakegamedesign/gamebuino/website.png" alt="Image du site officiel de gamebuino">
</center>
Ensuite, sur le site sont disponibles des jeux proposés par la communauté. Ce qui permet de s’amuser davantage mais aussi de pouvoir étendre ses connaissances. Le partage est donc une valeur prônée par la Gamebuino. Cette philosophie ne peut que contribuer à leur réussite.


En bref, la Gamebuino est une console existant sous la forme de deux versions, la meta et la classique. La Meta a un design vintage qui la rend plus unique. L’apprentissage et le perfectionnement dans de nombreux domaines de l’informatique, de l’électronique ou encore de la mécanique la rendent très intéressante. Sa limite est votre imagination. Cependant malgré un site et une communauté sympathique, la majorité des ressources sont anglaises. Cela peut être déstabilisant pour débuter mais en programmation on rencontre de nombreuses documentations en anglais; c’est une habitude à prendre. Elle est destinée plutôt à des personnes intéressées par la programmation. Des fonctionnalités supplémentaires comme un multijoueur dans la console pourraient étendre ses capacités et la rendre plus amusante. Alors si soutenir ce projet vous intéresse, vous savez ce qu’il vous reste à faire : [https://shop.gamebuino.com/fr/ ](https://shop.gamebuino.com/fr/ )

Voici quelques liens pour suivre et soutenir le projet :

[https://shop.gamebuino.com/fr/](https://shop.gamebuino.com/fr/)

[https://gamebuino.com/](https://gamebuino.com/)

[https://discord.gg/GbSkRyU](https://discord.gg/GbSkRyU)

[https://www.youtube.com/c/GamebuinoOfficial](https://www.youtube.com/c/GamebuinoOfficial)

[https://www.instagram.com/gamebuino/](https://www.instagram.com/gamebuino/)

[https://www.facebook.com/thegamebuino/](https://www.facebook.com/thegamebuino/)

[https://twitter.com/thegamebuino](https://twitter.com/thegamebuino)


Voici quelques ressources pour en savoir plus sur le projet :

[https://gamebuino.com/](https://gamebuino.com/)

[https://www.kickstarter.com/projects/rodot/gamebuino-meta?ref=nav_search&result=project&term=gamebuino](https://www.kickstarter.com/projects/rodot/gamebuino-meta?ref=nav_search&result=project&term=gamebuino)

[http://legacy.gamebuino.com/wiki/index.php?title=Main_Page](http://legacy.gamebuino.com/wiki/index.php?title=Main_Page)

En bonus, un jeu dans le thème rétro :
<center>
    <iframe src="https://www.silvergames.com/fr/pacman/iframe" width="450" height="500" style="margin:0;padding:0;border:0"></iframe>
</center>
