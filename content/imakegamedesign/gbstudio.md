Title: GB Studio
Date: 2021-03-19 11:00
Tags: jeux vidéos, moteur de jeux vidéos, open source, game boy, piskel
Category: iMakeGameDesign
Authors: Victor Lohézic
Summary: Pour les créateurs de jeux vidéos nostalgiques de la Game Boy GB studio est le logiciel qu'il vous faut.
Image: images/imakegamedesign/gbstudio/thumbnail.png
Type: Article
Status: published

Pour les créateurs de jeux vidéos nostalgiques de la Game Boy, <b>GB studio</b> est le logiciel qu'il vous faut. Ce logiciel gratuit sous <b>[license MIT](https://fr.wikipedia.org/wiki/Licence_MIT)</b> permet de créer son propre jeu. Il n'est pas nécessaire d'avoir de connaissances dans un langage de programmation puisqu'une interface visuelle permet de créer les mécaniques de son jeu. Pour créer les images de votre jeu, vous pouvez utiliser le logiciel avec lequel vous êtes le plus à l'aise comme [Piskel](https://popartech.com/piskel-creation-dun-personnage-de-jeux-video.html), Tiled, Asperite ou encore Photoshop... En plus, il est disponible sur <b>Windows</b>, <b>Linux</b> et <b>Mac OS</b>. Vous pouvez le télécharger [ici](https://chrismaltby.itch.io/gb-studio).
<br>
<br>
<center>
  <iframe width="787" height="443" src="https://www.youtube.com/embed/V9eV4i9kIt0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>
<br>
<br>
<b>Comment utilise-t-on ce logiciel ?</b>
<br>
Il y a la <b>documentation officielle</b> mais il n'y a pas de traduction française : [https://www.gbstudio.dev/docs](https://www.gbstudio.dev/docs)
<br>
<br>
Néanmoins, le <b>Bricolab de l'Espace 89</b> propose une série de vidéos très intéressantes  en français pour mieux l'appréhender :
[https://youtube.com/playlist?list=PLkDZpqd__q2NKc36Auknl3O_4Mfrut0iG](https://youtube.com/playlist?list=PLkDZpqd__q2NKc36Auknl3O_4Mfrut0iG)
<br>
<br>
<b>En quel format puis-je exporter le logiciel ?</b>
<br>
Vous pourrez  exporter votre jeu sous le format ROM afin que vous puissiez jouer à votre jeu sur un émulateur ou même sur votre <b>Game Boy</b> (nécessite de le graver sur une carte). Plus classiquement, il est aussi possible de le mettre sur votre site web. Tout le monde pourra y jouer grâce à l'HTML5 et même depuis son smartphone.
<br>
<br>
<b>Le futur du logiciel ?</b>
<br>
Il y a une seconde version du logiciel qui est en beta. Malheureusement, elle ne dispose pas encore de sa propre documentation. C'est l'occasion de vous présenter les limites de la première version. En effet, dans cette nouvelle version, il est possible de créer son jeu en couleur. Vous n'êtes plus limité à une nuance de vert. Dans la première version, vous ne pouviez que développer un RPG 2D (du type jeu d'aventure) alors que maintenant vous pouvez aussi créer des <b>platformers</b>, <b>point</b> and <b>click games</b> and <b>shoot 'em up</b>.
Pour les plus impatients, vous pouvez tester la nouvelle version ici : [https://github.com/chrismaltby/gb-studio/tree/v2beta](https://github.com/chrismaltby/gb-studio/tree/v2beta)
<br>
<br>
<center>
  <iframe width="787" height="443" src="https://www.youtube.com/embed/Wk96Pf6OL8Y" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>
<br>
<br>
<b>Exemples de ce qu'il est possible de faire</b>
<br>
<br>
<center>
  <iframe width="787" height="443" src="https://www.youtube.com/embed/4PCDXmg__HQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>
<br>
<b>Conclusion</b>
<br>
Un logiciel qui semble prometteur dont l'évolution est à suivre.
<br>
Maintenant, à vos clavier et souris et surtout amusez-vous bien !
