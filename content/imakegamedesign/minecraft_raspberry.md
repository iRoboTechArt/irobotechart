Title: Jouer à Minecraft sur Raspberry Pi à plusieurs
Date: 2019-04-06 22:40
Tags: python, iRoboTechArt
Category: iMakeGameDesign
Authors: V.Le Moual
Summary: Apprenons à développer un serveur Minecraft sur Raspberry Pi afin de pouvoir jouer à plusieurs.
Image: images/imakegamedesign/raspberrypi_minecraft/thumbnail.png
Type: Article
Status: published

<b><u><h5>C’est quoi le jeu Minecraft ? :</h5></u></b>

<span class="text-bleu">C'est une bonne question ! En effet, les joueurs sont plongés dans un monde constitué d’un immense bac à sable  qui permet des possibilités quasi-infinies. On peut y faire des constructions, de la survie, ou encore développer des mécanismes.
    Minecraft offre aux joueurs de très nombreuses occupations.</span>

<center><br><img class="responsive-img" width="50%" src="images/imakegamedesign/raspberrypi_minecraft/map.png" alt="Carte Minecraft"></center>

<b><u><h5>Un serveur Minecraft sur Raspberry Pi pourquoi faire ?</h5></u></b>




<span class="text-bleu">Cela permet au joueur de se connecter sur un même monde et aussi de pouvoir créer des constructions imaginées par les joueurs… On peut personnaliser le monde avec des plugins</span> (un [mod](https://www.minecraft-france.fr/les-mods/) agissant uniquement côté serveur et visant à modifier le gameplay sur le serveur Minecraft).

<center><br><img class="responsive-img" width="50%" src="https://irobotechart.com/images/imakegamedesign/raspberrypi_minecraft/thumbnail.png" alt="Image minecraft et Raspberry Pi"></center>

<b><u><h5>Installation du serveur Minecraft sur Raspberry Pi ?</h5></u></b>

<center><br><img class="responsive-img" width="25%" src="images/imakegamedesign/raspberrypi_minecraft/raspberry_pi.png" alt="Image du Raspberry Pi"></center>

<i><b><u><h6>Outil :</h6></u></b></i>
Les logiciels utilisés pour l’installation des programmes sont :
FileZilla pour transférer les fichiers et les programmes nécessaires.

PuTTy permet de commander à distance le Raspberry Pi par des lignes de commande.

<span id="rouge">Ces deux logiciels nécessitent de connaître l’adresse IP du Raspberry Pi.</span>

<i><b><u><h6>Liens des fichiers utilisés :</h6></u></b></i>
[https://mega.nz/#!rFlUDAKD!sLJjHvIXTvSiNSrqtYf8WfKBQ__CPHFylT8Naq1B6ws ](https://mega.nz/#!rFlUDAKD!sLJjHvIXTvSiNSrqtYf8WfKBQ__CPHFylT8Naq1B6ws )

<i><b><u><h6>Plugins :</h6></u></b></i>
[https://mega.nz/#F!6M1x3aqb!0YlNM3VQ03Dmpe2l1fomPw ](https://mega.nz/#F!6M1x3aqb!0YlNM3VQ03Dmpe2l1fomPw )

<i><b><u><h6>Opérations :</h6></u></b></i>

<ol>
<li><b>1-</b>Lancer le logiciel préinstallé PuTTy. Puis mettre l’@IP( adresse IP) dans l’encadré HostName(ou IP address) en SSH et open.</li>
<br>
<li><b>2-</b>Mettre le login PI et le mot de passe Raspberry puis appuyer sur la touche entrée. Voila vous êtes dans le terminal du Raspberry. Ensuite pour la seconde étape, il faut suivre l’instruction du fichier uploadé sur mega dont le lien se situe au début.</li>
<br>
<li><b>3-</b>Lancer le logiciel préinstallé FileZilla puis mettre l’@IP dans le gestionnaire de sites dans l’onglet fichier ou appuyer sur Ctrl+s. Cliquer sur nouveau site ensuite modifier le protocole en mettant SFTP. Puis  insérer l’@IP dans l’hôte. Pour finir, insérer l’identifiant PI et le mot de passe Raspberry. Enfin cliquer sur connexion.</li>
<br>
<li><b>4-</b>Aller dans le <i>home/pi/Document/minecraft/plugins</i> puis y ajouter ces différents plugins (lien ci-dessus)</li>.
<br>
</ol>

<style>
    .text-bleu,h6{
        color: #8BACD4;
        }
    #rouge{
        color: red;
        }
</style>
