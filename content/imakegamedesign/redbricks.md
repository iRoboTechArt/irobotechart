Title: RedBricks et le jeu vidéo libre
Date: 2018-11-26 20:30
Tags: jeux vidéos, robot
Category: iMakeGameDesign
Authors: Victor Lohézic
Summary: RedBricks est une plateforme de création communautaire dédiée au jeu vidéo. Son but est de remettre le joueur au coeur du processus créatif.
Image: images/imakegamedesign/redbricks/thumbnail.png
Type: Article
Status: published


Le 17 et 18 novembre 2018 a eu lieu à Toulouse le [Capitole du Libre](https://2018.capitoledulibre.org/). Durant cet événement, il y a eu des conférences autour de thématiques techniques : C++, développement Web, embarqué, DevOps; de logiciels libres de création graphique et multimédia; d'enjeux du logiciel libre dans la société et des communautés. Il y en avait aussi sur le jeu vidéo libre. J'ai donc découvert la société <b>Otterways</b>. Elle développe <b>un projet nommé RedBricks</b>. RedBricks est une plateforme de création communautaire dédiée au jeu vidéo. Son but est de <b>remettre le joueur au coeur du processus créatif</b>.
<br>
<br>

<center>
    <img class="responsive-img" src="images/imakegamedesign/redbricks/thumbnail.png" width="25%" alt="Jeux vidéo">
</center>


Pour mieux comprendre leur projet, sur leur [site](https://redbricks.games/home) est disponible la vidéo suivante :
<br>
<br>

<div class="video-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/7TXW5JX8Nx4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<br>
<br>
Vous ne connaissez pas de <b>jeux libres</b> ? En voici quelques uns parmi les plus connus pour vous prouver leur existence :
[Minetest](https://www.minetest.net/), [Hedgewars](https://www.hedgewars.org/fr), [Teeworlds](https://www.teeworlds.com/)...

<br>
RedBricks est justement né de l'envie de créer des jeux vidéo libres dans un cadre professionnel tout en devant développer un modèle économique viable. Si le sujet vous intéresse, je vous propose donc de visionner la conférence d'Aline Bardou nommée <i>"Créer et financer un jeu vidéo libre, une nouvelle approche"</i> :
<br>
<br>
<div class="video-container">
    <iframe width="729" height="410" src="https://www.youtube.com/embed/XfUrvKV_BYo?list=PLU8kgF3be3rSyt1WzyqelEswLdiyhm66N" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<br>
Maintenant, je vous souhaite bon jeu ;)
