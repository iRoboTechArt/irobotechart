Title: Un Robot Chat OpenSource
Date: 2018-03-01 13:42
Tags: opencat, cat,
Category: iRoboTechArt
Authors: Victor Lohézic
Summary: Les chats seraient l'animal de compagnie préféré des français. Cela promet un bel avenir en France pour le robot OpenCat de Rongzhong Li. N'est ce pas ?
Image: images/irobotechart/opencat/thumbnail-cat-apple.jpg
Type: Article
Status: published

Les chats seraient l'animal de compagnie préféré des français. Cela promet un bel avenir en France pour le robot OpenCat de Rongzhong Li.
<center>
    <img class="responsive-img" src="images/irobotechart/opencat/cat-apple.jpg" alt="Image d'Opencat, le chat robot est en train de marcher devant une pomme">
</center>
Rongzhong Li travaille actuellement comme assistant professeur. Il a aussi démarré une entreprise avec des projets robotiques. Il a conçu un robot en forme de chat. Pour élaborer ce robot, il a utilisé un Arduino et un Raspberry Pi. L'Arduino est un microcontrôleur. Le raspberry pi est un nano ordinateur. Il a choisi ces deux technologies pour des usages différents. En effet, l'arduino contrôle directement les mouvements du robot avec des servo-moteurs. Les servo-moteurs sont des moteurs dont on peut contrôler la position, l'angle. Tandis que le Raspberry Pi n'en prend pas la responsabité. Il peut cepedant envoyer des ordres à l'arduino. Sa principale fonction est de répondre à des questions comme "Qui suis-je ?" "D'où je viens ?" et "Ou vais-je ?". Rongzhong Li dit même que son robot possède un certain instinct qui l'empecherait de sauter d'une falaise.

Sa structure élastique permet d'absorber les chocs et de protéger l'électronique. Son robot est aussi agile qu'un chat, mieux qu'un <a href="cymru_dragon_tan.html" class="url_hide">dragon</a>.  A ce jour, il existe deux prototypes.
<center>
    <img class="responsive-img" src="images/irobotechart/opencat/cats.jpg" alt="Image d'un chat qui monte des escaliers'">
</center>
Pour l'équillibre du robot et pour différentes fonctions, il a développé une intelligence artificielle. La vidéo suivante illustre l'évolution datée de son travail  :

<div class="video-container">
    <iframe width="854" height="480" src="https://www.youtube.com/embed/iGEdF5QsDB8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
<br>

Rongzhong Li a programmé l'arduino en C et le Raspberry Pi en Python. Lors de ce projet, sa principale motivation est de développer un robot sur lequel des personnes de différents âges, métiers, origines, puissent collaborer et y trouver un interêt. Il désire que les enfants puissent apprendre la physique et la programmation. Il aimerait que les experts de la robotique puisse contribuer à baisser son coût. Il espère aussi que les ingénieurs puisse améliorer l'intelligence artificiel. Maintenant, connaissez-vous ces deux robots  ?


Boston Dynamic Dogs

<div class="video-container">
    <iframe width="854" height="480" src="https://www.youtube.com/embed/RYzn_gmFs5w" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
<br>
<div class="video-container">
    <iframe width="854" height="480" src="https://www.youtube.com/embed/fUyU3lKzoio" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
<br>
Sony Aibo

<div class="video-container">
    <iframe width="1366" height="768" src="https://www.youtube.com/embed/lhESLovHII4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>

Leur principal inconvénient est leur prix. En effet, ils ne sont pas vraiment abordables. Ainsi, il souhaite proposer une alternative.


Si le projet connaît du succès, il proposera un commercialisation. Si cela peut vous rassurer, en ce moment même, sa boite mail est remplie. Enfin, il accorde une grande confiance à la communauté des makers pour le développement de OpenCat. A vous de choisir, si vous voulez aider l'ascension d'OpenCat.

<center>
    <img class="responsive-img" src="images/irobotechart/opencat/climb-ladder.jpg" alt="Image d'Opencat, le chat robot est en train de marcher devant une pomme">
</center>
Pour en savoir plus :

[http://borntoleave.github.io](http://borntoleave.github.io)

[https://www.linkedin.com/in/rongzhongli/](https://www.linkedin.com/in/rongzhongli/)

[https://create.arduino.cc/projecthub/petoi/opencat-845129?ref=platform&ref_id=424_trending___&offset=0](https://create.arduino.cc/projecthub/petoi/opencat-845129?ref=platform&ref_id=424_trending___&offset=0)

Toutes les images utilisées dans cet article ont eu l'accord de Rongzhong Li.
