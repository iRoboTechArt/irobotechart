Title: Une machine à musique
Date: 2018-12-20 20:25
Tags: music
Category: iRoboTechArt
Authors: Victor Lohézic
Summary: Vous n'avez pas d'idées pour votre prochain projet, voici une « Marble Machine » qui pourrait vous inspirer.
Image: images/irobotechart/marble_machine/thumbnail.png
Type: Article
Status: published

Vous n'avez pas d'idées pour votre prochain projet, voici une « Marble Machine » qui pourrait vous inspirer.

Martin Molin (Wintergartan) est un artiste suédois qui a créé une machine qui joue de la musique. Dans la seconde vidéo, il explique comment faire la "Music Box". Il travaille maintenant sur une nouvelle machine ["Marble Machine X](https://www.youtube.com/playlist?list=PLLLYkE3G1HED6rW-bkliHbMroHYFf4ukv). Enfin, il met toujours en ligne des vidéos alors n'hésitez pas à suivre [ses projets](https://www.youtube.com/channel/UCcXhhVwCT6_WqjkEniejRJQ).

<br>
 <div class="video-container">
        <iframe width="621" height="321" src="https://www.youtube.com/embed/IvUU8joBb1Q" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
<br>
 <div class="video-container">
        <iframe width="642" height="361" src="https://www.youtube.com/embed/OacXT6Nk0LE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
