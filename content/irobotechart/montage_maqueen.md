Title: Montage Maqueen
Date: 2021-02-23 16:00
Tags: irobotechart, PopArTech, robotique, iRoboTechArt, micro:bit, robot, python
Category: iRoboTechArt
Authors: Victor Lohézic
Summary: Nous allons monter le <b>robot [Maqueen](https://youtu.be/5bBTvs9sjAo)</b>. Il fonctionne avec la carte [micro:bit](https://microbit.org/fr/).
Image: images/irobotechart/montage_maqueen/thumbnail.jpg
Type: Article
Status: published

Nous allons monter le <b>robot [Maqueen](https://youtu.be/5bBTvs9sjAo)</b>. Il fonctionne avec la carte [micro:bit](https://microbit.org/fr/).

  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Matériel 1</b>
                        <img align="right" class="responsive-img materialboxed" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Le kit Maqueen :
            <center><br><img class="responsive-img" width="50%" src="images/irobotechart/montage_maqueen/kit_maqueen.jpg" alt="Kit Maqueen"></center>
          </span>
          Il est constitué d'un chassis, de deux roues et de leur pneu, un boîtier de pile et d'une surface adhésive, un capteur ultrason.  
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Matériel 2</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Il faudra aussi prévoir l'achat d'une carte micro:bit, de piles, et d'un cable pour téléverser le code :
            <center><br><img class="responsive-img" width="50%" src="images/irobotechart/montage_maqueen/microbit.jpg" alt="micro:bit"></center>
          </span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Etape 1</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Assemble les pneus avec les roues :
            <center><br><img class="responsive-img" width="50%" src="images/irobotechart/montage_maqueen/pneus1.jpg" alt="pneus et roues"></center>
            <center><br><img class="responsive-img" width="50%" src="images/irobotechart/montage_maqueen/pneus2.jpg" alt="pneus et roues"></center>
          </span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Etape 1</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Assemble les roues au chassis :
            <center><br><img class="responsive-img" width="50%" src="images/irobotechart/montage_maqueen/roue_chassis.jpg" alt="Assemblage de la roue au chassis"></center>
          </span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Etape 2</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Ajoute le boîtier de pile :
            <center><br><img class="responsive-img" width="50%" src="images/irobotechart/montage_maqueen/boitier.jpg" alt="Assemblage du boîtier de piles"></center>
            <center><br><img class="responsive-img" width="50%" src="images/irobotechart/montage_maqueen/boitier2.jpg" alt="Assemblage du boîtier de piles"></center>
            <center><br><img class="responsive-img" width="50%" src="images/irobotechart/montage_maqueen/boitier3.jpg" alt="Assemblage du boîtier de piles"></center>
            <center><br><img class="responsive-img" width="50%" src="images/irobotechart/montage_maqueen/boitier4.jpg" alt="Assemblage du boîtier de piles"></center>
            <center><br><img class="responsive-img" width="50%" src="images/irobotechart/montage_maqueen/boitier5.jpg" alt="Assemblage du boîtier de piles"></center>
            <center><br><img class="responsive-img" width="50%" src="images/irobotechart/montage_maqueen/boitier6.jpg" alt="Assemblage du boîtier de piles"></center>
          </span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Etape 3 </b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Insère la micro:bit
            <center><br><img class="responsive-img" width="50%" src="images/irobotechart/montage_maqueen/microbit_robot.jpg" alt="Insertion de la micro:bit"></center>
          </span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Etape 4</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Ajoute le capteur ultrason
            <center><br><img class="responsive-img" width="50%" src="images/irobotechart/montage_maqueen/montage fini.jpg" alt="Ajout du capteur ultrason"></center>
          </span>
      </div>
    </div>
  </div>
  Voilà le robot est monté, il n'y a plus qu'à le tester. Pour cela, rends-toi à l'adresse suivante : [https://makecode.microbit.org](https://makecode.microbit.org).
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Etape 5</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Crée un nouveau projet
            <center><br><img class="responsive-img" width="75%" src="images/irobotechart/montage_maqueen/test1.png" alt="Création d'un nouveau projet"></center>
            <center><br><img class="responsive-img" width="75%" src="images/irobotechart/montage_maqueen/test2.png" alt="Création d'un nouveau projet"></center>
          </span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Etape 6</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Clique sur l'onglet python (pour plus de fun ;)
            <center><br><img class="responsive-img" width="75%" src="images/irobotechart/montage_maqueen/test3.png" alt="Onglet Python"></center>
          </span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Etape 7</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Ajoute l'extension Maqueen
            <center><br><img class="responsive-img" width="75%" src="images/irobotechart/montage_maqueen/test4.png" alt="Ajout de l'extension"></center>
            <center><br><img class="responsive-img" width="75%" src="images/irobotechart/montage_maqueen/test5.png" alt="Ajout de l'extension"></center>
            <center><br><img class="responsive-img" width="75%" src="images/irobotechart/montage_maqueen/test6.png" alt="Ajout de l'extension"></center>
          </span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Etape 8</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Code les lignes pour faire avancer le robot
            <center><br><img class="responsive-img" width="75%" src="images/irobotechart/montage_maqueen/test7.png" alt="programmation"></center>
            <center><br><img class="responsive-img" width="75%" src="images/irobotechart/montage_maqueen/test8.png" alt="programmation"></center>
          </span>
      </div>
    </div>
  </div>
  Le code fini :
  <br>
  <div style="position:relative;height:calc(300px + 5em);width:100%;overflow:hidden;"><iframe style="position:absolute;top:0;left:0;width:100%;height:100%;" src="https://makecode.microbit.org/---codeembed#pub:_MwV0HTbfFPoc" allowfullscreen="allowfullscreen" frameborder="0" sandbox="allow-scripts allow-same-origin"></iframe></div>
  <br>
  <br>
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Etape 9</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Télécharge le code
            <center><br><img class="responsive-img" width="75%" src="images/irobotechart/montage_maqueen/test9.png" alt="téléchargement"></center>
          </span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Etape 10</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Glisse le fichier .hex dans le dossier de la micro:bit après l'avoir connecté à l'ordinateur grâce au câble usb
            <center><br><img class="responsive-img" width="75%" src="images/irobotechart/montage_maqueen/test10.png" alt="téléversement"></center>
          </span>
      </div>
    </div>
  </div>
   Et voilà le robot avance !
   <br>"- Rapide comme l’éclair ?
   <br>- Puissant comme la foudre !" <i>Cars</i>
  <br>
  <br>
  <div class="video-container">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/vPjN2y8COOc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>
  <br>
