Title: La Gazette du Web
Date: 2021-02-05 13:42
Tags: gazette, web, arduino, robotique, programmation, art, pixel
Category: iRoboTechArt
Authors: Victor Lohézic
Summary: La Gazette du Web regroupe des articles, vidéos trouvés sur le web qui peuvent ensuite donner des idées de projets.
Image: images/irobotechart/gazetteduweb/thumbnail.gif
Type: Article
Status: published

<p>La Gazette du Web regroupe des articles, vidéos trouvés sur le web qui peuvent ensuite donner des idées de projets.</p>

<div class="row">
        <div class="col s12 m4">
          <div class="card">
            <div class="card-image">
 <div class="carousel carousel-slider center" data-indicators="true">

    <div class="carousel-item red white-text" href="#one!">
      <h2>Robot Dessinateur</h2>
      <a class="btn waves-effect white grey-text darken-text-2" href="https://quai-lab.com/the-drawing-bot-tuto-13/">Voir</a><br><br>
      <center><div class="tenor-gif-embed" data-postid="13167883" data-share-method="host" data-width="75%" data-aspect-ratio="1.3315508021390374"><a href="https://tenor.com/view/love-drawing-cute-gif-13167883">Love Drawing GIF</a> from <a href="https://tenor.com/search/love-gifs">Love GIFs</a></div><script type="text/javascript" async src="https://tenor.com/embed.js"></script></center>
    </div>

    <div class="carousel-item amber white-text" href="#two!">
    <h2>Robot Quadrupède Lego</h2>
    <a class="btn waves-effect white grey-text darken-text-2" href="https://create.arduino.cc/projecthub/tartrobotics/quadruped-robot-30f8d9?ref=platform&ref_id=424_trending___&offset=9">Voir</a><br><br>
    <center><div class="tenor-gif-embed" data-postid="14541584" data-share-method="host" data-width="75%" data-aspect-ratio="1.0618336886993602"><a href="https://tenor.com/view/bionicle-toa-dance-fortnite-fortnite-dance-gif-14541584">Bionicle Toa GIF</a> from <a href="https://tenor.com/search/bionicle-gifs">Bionicle GIFs</a></div><script type="text/javascript" async src="https://tenor.com/embed.js"></script></center>
    </div>
    <!--
    <div class="carousel-item green white-text" href="#three!">
      <h2>Third Panel</h2>
      <p class="white-text">This is your third panel</p>
    </div>
    <div class="carousel-item blue white-text" href="#four!">
      <h2>Fourth Panel</h2>
      <p class="white-text">This is your fourth panel</p>
    </div>-->
  </div>
            </div>
            <div class="card-content">
              <center><h5>iRoboTechArt</h5></center>
            </div>
          </div>
        </div>
        <div class="col s12 m4">
          <div class="card">
            <div class="card-image">
 <div class="carousel carousel-slider center" data-indicators="true">

    <div class="carousel-item red white-text" href="#one!">
      <h2>Baguette Magique Arduino</h2>
      <a class="btn waves-effect white grey-text darken-text-2" href="https://www.youtube.com/watch?v=yDwRiH3Mfek">Voir</a><br><br>
      <center><div class="tenor-gif-embed" data-postid="20163217" data-share-method="host" data-width="75%" data-aspect-ratio="1.0"><a href="https://tenor.com/view/harry-potter-harry-wizard-mago-dan-hernandez-gif-20163217">Harry Potter Wizard GIF</a> from <a href="https://tenor.com/search/harrypotter-gifs">Harrypotter GIFs</a></div><script type="text/javascript" async src="https://tenor.com/embed.js"></script></center>
    </div>

    <div class="carousel-item amber white-text" href="#two!">
    <h2>Horloge à LED en bois</h2>
    <a class="btn waves-effect white grey-text darken-text-2" href="https://create.arduino.cc/projecthub/andrew-jones/making-a-wooden-led-clock-84b9ea?ref=platform&ref_id=424_trending___&offset=341">Voir</a><br><br>
    <center><div class="tenor-gif-embed" data-postid="11676332" data-share-method="host" data-width="75%" data-aspect-ratio="1.6824324324324327"><a href="https://tenor.com/view/pongo-yay-101dalmatians-clock-countdown-gif-11676332">Pongo Yay GIF</a> from <a href="https://tenor.com/search/pongo-gifs">Pongo GIFs</a></div><script type="text/javascript" async src="https://tenor.com/embed.js"></script></center>
    </div>
   <!--
    <div class="carousel-item green white-text" href="#three!">
      <h2>Third Panel</h2>
      <p class="white-text">This is your third panel</p>
    </div>
    <div class="carousel-item blue white-text" href="#four!">
      <h2>Fourth Panel</h2>
      <p class="white-text">This is your fourth panel</p>
    </div>-->
  </div>
            </div>
            <div class="card-content">
              <center><h5>iTech3D</h5></center>
            </div>
          </div>
        </div>
        <div class="col s12 m4">
          <div class="card">
            <div class="card-image">
 <div class="carousel carousel-slider center" data-indicators="true">

    <div class="carousel-item red white-text" href="#one!">
      <h2>Minecraft et Python</h2>
      <a class="btn waves-effect white grey-text darken-text-2" href="https://youtube.com/playlist?list=PL6JtJ0Q7T-YxOr2vCJesziB87qytgyz1N">Voir</a><br><br>
      <center><div class="tenor-gif-embed" data-postid="18518130" data-share-method="host" data-width="75%" data-aspect-ratio="1.3795013850415512"><a href="https://tenor.com/view/minecraft-gif-18518130">Minecraft GIF</a> from <a href="https://tenor.com/search/minecraft-gifs">Minecraft GIFs</a></div><script type="text/javascript" async src="https://tenor.com/embed.js"></script></center>
    </div>

    <div class="carousel-item amber white-text" href="#two!">
    <h2>Jeux de Plateforme</h2>
    <a class="btn waves-effect white grey-text darken-text-2" href="https://youtube.com/playlist?list=PLqihHB3m2p01A2U9fjkgVI32zSeyYoWJ3">Voir</a><br><br>
    <center><div class="tenor-gif-embed" data-postid="13061046" data-share-method="host" data-width="100%" data-aspect-ratio="1.7913669064748199"><a href="https://tenor.com/view/dipper-gravity-falls-game-gif-13061046">Dipper Gravity GIF</a> from <a href="https://tenor.com/search/dipper-gifs">Dipper GIFs</a></div><script type="text/javascript" async src="https://tenor.com/embed.js"></script></center>
    </div>

    <!--
    <div class="carousel-item green white-text" href="#three!">
      <h2>Third Panel</h2>
      <p class="white-text">This is your third panel</p>
    </div>
    <div class="carousel-item blue white-text" href="#four!">
      <h2>Fourth Panel</h2>
      <p class="white-text">This is your fourth panel</p>
    </div>-->
  </div>
            </div>
            <div class="card-content">
              <center><h5>iMakeGame</h5></center>
            </div>
          </div>
        </div>
      </div>
<br>

<script>
$('.carousel-slider').carousel({fullWidth: true, padding:0},setTimeout(autoplay, 4500));
function autoplay() {
  $('.carousel').carousel('next');
  setTimeout(autoplay, 7500);
   }
</script>
