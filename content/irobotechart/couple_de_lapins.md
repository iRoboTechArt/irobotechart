Title: <br>Enigme avec des lapins
Date: 2018-10-21 18:30
Tags: programmation, mathématiques, énigme, python
Category: iRoboTechArt
Authors: Victor Lohézic
Summary: « Un homme met un couple de lapins dans un lieu isolé de tous les côtés par un mur. Combien de couples obtient-on en un an si chaque couple engendre tous les mois un nouveau couple à compter du troisième mois de son existence ? »
Image: images/irobotechart/couple_de_lapins/thumbnail.png
Type: Article
Status: published

<b>Voici une petite énigme qui mélange programmation et mathématiques :</b>
<br>

Un homme met un couple de lapins dans un lieu isolé de tous les côtés par un mur. Combien de couples obtient-on en deux ans si chaque couple engendre tous les mois un nouveau couple à compter du troisième mois de son existence ?
<br>
<br>
Pour répondre, vous pouvez écrire un programme en python. Vous êtes libre d'utiliser toutes les structures qui vous paraissent intéressantes (par exemple les conditions ou boucles). Cependant pour valider votre réponse, à la fin vous devez afficher votre résulat sous la forme d'un entier:

    réponse = 5
    print(réponse)

Bonne chance !
<center>
    <img class="responsive-img" width="50%" src="images/irobotechart/couple_de_lapins/thumbnail.png" alt="Logo iRoboTechArt">
</center>

<iframe width=100% frameborder=0 scrolling=no allowtransparency=true style=visibility:hidden src=https://tech.io/playground-widget/7ae70f79b588ad71eaf5a33cf8daa0de90543/welcome/519485/Enigme></iframe>
<script>if(void 0===window.techioScriptInjected){window.techioScriptInjected=!0;var d=document,s=d.createElement("script");s.src="https://files.codingame.com/codingame/iframe-v-1-4.js",(d.head||d.body).appendChild(s)}</script>
