Title: Festival Interceltique 2019 Go !
Date: 2019-08-02 11:32
Tags: association, festival interceltique de lorient, FIL, crepart, iRoboTechArt
Category: iCrepArtDesign
Authors: l’équipe iRTA-CrepArt
Summary: Et c’est parti pour le <b>Festival Interceltique cet été du 2 au 11 Août 2019</b> avec à l’honneur cette année La <b>Galice</b> ! Un 49ème Festival avec comme chaque année une programmation riche, variée et exceptionnelle. Et donc à ne rater sous aucun prétexte !<br>
Image: images/icrepartdesign/crepart_fil/thumbnail.jpg
Type: Article
State: published




Et c’est parti pour le <b>Festival Interceltique cet été du 2 au 11 Août 2019</b> avec à l’honneur cette année La <b>Galice</b> ! Un 49ème Festival avec comme chaque année une programmation riche, variée et exceptionnelle. Et donc à ne rater sous aucun prétexte !<br>
[](https://www.festival-interceltique.bzh/)<br>
<center>
    <img src="https://irobotechart.com/images/icrepartdesign/crepart_fil/affichefil19.jpg" width="35%"  alt="affiche festival interceltique">
</center>
<br>
En plus ça tombe bien puisque nous y serons aussi ! avec le <b>PROjet CrepArt.</b> <br>
Et nous nous retrouverons à l’espace <b>VillageSolidaire</b> premier stand sur la gauche en entrant. Nous rater est impossible ! ;-)<br>
Alors à très bientôt…<br><br>

<b>L’équipe iRTA-CrepArt</b><br>
Louis, Rachel, Yuna, Achille, Thao, Leïna, Maïjane, Lancelot, Perig, Natalia, Martin, …

<center>
	<img src="images/icrepartdesign/crepart_fil/image.PNG"  width="100%" alt="Image de crêpe art">
</center>
