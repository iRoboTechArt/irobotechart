Title:  Robot avec MagicaVoxel
Date: 2021-02-25 19:00
Tags: dessin,  pixel, irobotechart, PopArTech, MagicaVoxel, robot
Category: iPixelArtAnim
Authors: Victor Lohézic
Summary: Nous allons dessiner un petit robot dans le logiciel MagicaVoxel.
Image: images/ipixelartanim/robot_magicavoxel/thumbnail.png
Type: Article
Status: published

Nous allons dessiner en 3D un <b>robot</b> dans le logiciel <b>MagicaVoxel.</b> Si tu ne connais pas ce logiciel, je t'invite à lire cet article : [MagicaVoxel](https://popartech.com/magicavoxel.html). Si tu souhaites t'entraîner un peu, cet article pourra t'aider : [Logo PopArTech](https://popartech.com/logo-avec-magicavoxel.html).
<br>
<br>
Cet article sera composé de petites missions.

  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Mission 1</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Dessine les jambes du robot :
            <center><br><img class="responsive-img" width="50%" src="images/ipixelartanim/robot_magicavoxel/legs.png" alt="Jambes"></center>
          </span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Mission 2</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Dessine le corps du robot:
            <center><br><img class="responsive-img" width="50%" src="images/ipixelartanim/robot_magicavoxel/body1.png" alt="Bas du corps"></center>
            <center><br><img class="responsive-img" width="50%" src="images/ipixelartanim/robot_magicavoxel/body2.png" alt="Haut du corps"></center>            
          </span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Mission 3</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Dessine les "oreilles" puis déplace-les au dessus du corps du personnage :
            <center><br><img class="responsive-img" width="50%" src="images/ipixelartanim/robot_magicavoxel/ears.png" alt="oreilles"></center>
              <center><br><img class="responsive-img" width="50%" src="images/ipixelartanim/robot_magicavoxel/ears2.png" alt="oreilles au dessus du corps"></center>
            </span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Mission 4</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Crée de la tête :
            <center><br><img class="responsive-img" width="50%" src="images/ipixelartanim/robot_magicavoxel/head.png" alt="oreilles"></center>
          </span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Mission 5</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Peins ton robot pour lui donner vie :
            <center><br><img class="responsive-img" width="50%" src="images/ipixelartanim/robot_magicavoxel/blue1.png" alt="Peinture 1"></center>
            <center><br><img class="responsive-img" width="50%" src="images/ipixelartanim/robot_magicavoxel/blue2.png" alt="Peinture 2"></center>
            <center><br><img class="responsive-img" width="50%" src="images/ipixelartanim/robot_magicavoxel/blue3.png" alt="Peinture 3"></center>
            <center><br><img class="responsive-img" width="50%" src="images/ipixelartanim/robot_magicavoxel/blue4.png" alt="Peinture 4"></center>
        </span>
      </div>
    </div>
  </div>


  Bravo, tu as crée ton personnage dans MagicaVoxel !
  <br>
  <br>
  <div class="video-container">
      <iframe width="1194" height="618" src="https://www.youtube.com/embed/4jAADhaAUFc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>
  <br>
<center>
  <a class="waves-effect waves-light btn " href="https://drive.google.com/file/d/1DyGWvmWN69AZbG9ORDuBoV41NkGIPbGV/view?usp=sharing"><i class="material-icons left">download</i>Télécharger le fichier final</a>
</center>
