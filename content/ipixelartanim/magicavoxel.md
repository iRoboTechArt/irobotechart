Title: MagicaVoxel
Date: 2020-09-07 20:13
Tags: MagicaVoxel, iRoboTechArt, 3d, modélisation, pixel
Category: iPixelArtAnim
Authors: Victor Lohézic
Summary: Dans cet article, découvrez MagicaVoxel un outil gratuit et libre pour faire du pixel art en 3D.
Image: images/ipixelartanim/magicavoxel/thumbnail.jpg
Type: Article
Status: published

<b>MagicaVoxel est un logiciel de modélisation 3D. Il permet de créer des personnages, des paysages à l'aide de [Voxel](https://fr.wikipedia.org/wiki/Voxel).</b>
<br>
<br>
<center>

  <div class="slider">
    <ul class="slides">
    <li>
      <img src="images/ipixelartanim/magicavoxel/sample3.jpg">  <!-- random image -->
      <div class="caption left-align">
        <h3>MagicaVoxel</h3>
      </div>
    </li>
    <li>
      <img src="images/ipixelartanim/magicavoxel/sample2.jpg">  <!-- random image -->
    </li>
      <li>
        <img src="images/ipixelartanim/magicavoxel/sample1.jpg"> <!-- random image -->
      </li>
      <li>
        <img src="images/ipixelartanim/magicavoxel/sample4.jpg">  <!-- random image -->
      </li>
      <li>
        <img src="images/ipixelartanim/magicavoxel/sample6.jpg">  <!-- random image -->
      </li>
      <li>
        <img src="images/ipixelartanim/magicavoxel/sample7.jpg">  <!-- random image -->
      </li>
      <li>
        <img src="images/ipixelartanim/magicavoxel/sample8.png">  <!-- random image -->
      </li>
    </ul>
  </div>

</center>
<br>
Les <b>Voxels</b> sont des cubes (pixels en 3D) de couleurs. Son utilisation est simple,  à la manière du jeu [Minecraft](https://fr.wikipedia.org/wiki/Minecraft), il est possible de poser des blocs afin de créer des formes, mais aussi de les peindre, ou encore de jouer avec l'éclairage. C'est donc un outil puissant et accessible. Le logiciel développé par [Ephtracy](https://twitter.com/ephtracy?lang=fr) est libre et [téléchargeable](https://ephtracy.github.io/) sur les systèmes d'exploitation <b>Linux</b> et <b>Windows</b> gratuitement.

<br>
Ce logiciel permet donc de créer des <b>personnages</b> en 3D pour le développement de jeux vidéos. Il peut aussi être utilisé afin de modéliser un <b>bâtiment</b>, une <b>ville</b>, des paysages. Il peut aussi se montrer intéressant pour de la <b>réalité augmentée</b> ou <b>virtuelle</b>.

<br>
On peut trouver sur Youtube de nombreux [tutos](https://www.youtube.com/playlist?list=PLYmCMg3QL20-jQNEyAAmKjGteZDxVesmy) et réalisations :
<br><br>
<div class="video-container">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLYmCMg3QL20-jQNEyAAmKjGteZDxVesmy" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>


<br>
<div class="video-container">
  <iframe width="1194" height="672" src="https://www.youtube.com/embed/zrWUdaXvbP0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<br>

<div class="video-container">
  <iframe width="1194" height="672" src="https://www.youtube.com/embed/1TLNW8A-Hfg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<br>

<div class="video-container">
  <iframe width="1194" height="672" src="https://www.youtube.com/embed/yXcka7xmVl0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<br>

<div class="video-container">
  <iframe width="1194" height="672" src="https://www.youtube.com/embed/msi69zgJNx8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<br>
Pour mieux <b>appréhender le logiciel</b>, vous trouverez dans celui-ci plusieurs <b>exemples</b> de fichiers déjà réalisés.

<script>
document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('.slider');
  var instances = M.Slider.init(elems, options);
});

// Or with jQuery

$(document).ready(function(){
  $('.slider').slider();
});
</script>
