Title: Créer son smiley avec Piskel
Date: 2019-01-31 12:22
Tags: piskel, iRoboTechArt, smiley
Category: iPixelArtAnim
Authors: Victor Lohézic
Summary: Apprenons à créer notre propre smiley avec Piskel. Si l'inspiration ne vous manque pas, pourquoi ne pas créér des smileys pour iRoboTechArt ?
Image: images/ipixelartanim/piskel_smiley/smiley_robot_clin_doeil.gif
related_posts: piskel, smiley
Type: Article
Status: published

<b>Nous allons apprendre à créer un [smiley](https://fr.wikipedia.org/wiki/Smiley) avec [Piskel un logiciel libre de Pixel Art.](piskel-un-editeur-de-sprites.html)</b>
<br>
On va procéder étape par étape.

## Faire un croquis sur un papier

Avant de se lancer sur le logiciel, il est préférable de faire un croquis afin d'avoir une idée de notre smiley. Cela permet ensuite d'aller plus vite sur le logiciel. Et voilà un beau smiley robot ;)

<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/piskel_smiley/croquis.jpg" alt="Croquis du smiley" width="50%">
</center>
<br>

## Dessiner la forme du visage

Maintenant, on peut commencer à utiliser Piskel. Dessinons un rectangle.
<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/piskel_smiley/smiley_robot-1.png.png" alt="rectangle" width="50%">
</center>
<br>

## Les oreilles

<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/piskel_smiley/smiley_robot-2.png.png" alt="Oreille" width="50%">
</center>
<br>

## Les yeux
On va faire un oeil ouvert et l'autre fermé pour que notre smiley fasse un clin d'oeil.

<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/piskel_smiley/smiley_robot-3.png.png" alt="Les yeux" width="50%">
</center>
<br>

## La bouche

C'est mieux avec un sourire :)

<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/piskel_smiley/smiley_robot-4.png.png" alt="La bouchel" width="50%">
</center>
<br>

## Personnalisons-le

Pour lui donner un coté plus gentil, un pompon sera parfait !

<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/piskel_smiley/smiley_robot-5.png.png" alt="Avec un pompon, le robot est plus sympas :)" width="50%">
</center>
<br>

## En couleur

<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/piskel_smiley/smiley_robot-6.png.png" alt="En couleur, c'est mieux" width="50%">
</center>
<br>

Voilà notre smiley est fini. Je vous conseille d'essayer de refaire chaque étape pour vous approprier le logiciel.

<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/piskel_smiley/smiley_robot.gif" alt="Récapitulatif de toutes les étapes" width="50%">
</center>
<br>

Ensuite, il est très simple de l'animer ou encore de créer d'autres smileys à partir du robot.
Voici une petite astuce avant d'autres exemples (allez à 2min30) :
<br>
<div class="video-container">
    <iframe width="729" height="410" src="https://www.youtube.com/embed/y-dOHbswKLQ?t=154" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"   allowfullscreen></iframe>
</div>
<br>
### Clin d'oeil animé

<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/piskel_smiley/smiley_robot_clin_doeil.gif" alt="Smiley qui fait un clin d'oeil" width="50%">
</center>
<br>

### Sourire

<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/piskel_smiley/smiley_heureux.png" alt="Smiley qui sourit" width="50%">
</center>
<br>

### Sans émotion

<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/piskel_smiley/smiley_sans_emotion.png.png" alt="Smiley sans émotion" width="50%">
</center>
<br>

### Triste

<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/piskel_smiley/smiley_triste.png" alt="smiley qui est triste" width="50%">
</center>
<br>

### Très triste

<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/piskel_smiley/smiley_pleure.gif" alt="smiley qui est triste" width="50%">
</center>
<br>

### Amoureux

<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/piskel_smiley/amoureux.gif" alt="Smiley qui est amoureux" width="50%">
</center>
<br>

### A vous !

Pourquoi ne pas créer les smileys du site iRoboTechArt ? Si l'inspiration ne vous manque pas, créez des smileys à partir du logo iRoboTechArt ou des thèmes que sont la robotique, la technologie, l'art... Vos créations apparaîtront dans un prochain article avec un classement.

Envoyez-nous vos créations à l'adresse mail suivante : webmasters@irobotechart.com
