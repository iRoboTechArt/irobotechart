Title: Piskel, animation d'un personnage
Date: 2019-03-16 10:30
Tags: jeux vidéos, ennemie, bombe, piskel
Category: iPixelArtAnim
Authors: Victor Lohézic
Summary: Apprenons à animer un personnage dans Piskel.L'animation que nous allons réaliser s'appelle idle.
Image: images/ipixelartanim/piskel_animation_ennemi_bombe_idle/thumbnail.png
Type: Article
Status: published

<b>Vous allez apprendre à animer un personnage avec [Piskel un logiciel libre de Pixel Art.](piskel-un-editeur-de-sprites.html)</b>
<br>
Si vous ne maîtrisez pas ce logiciel, je vous invite à lire [cet article](creer-son-smiley-avec-piskel.html). Nous allons reprendre le personnage [du dernier article.](piskel-creation-dun-personnage-de-jeux-video.html) Nous allons réaliser l'animation <b>idle</b> de notre personnage. Cette animation sera utilisée <b>dans un jeu vidéo</b> lorsque notre personnage ne fera rien, ne bougera pas. Voici une petite vidéo avec des animations de développeurs créatifs :
<br>
<div class="video-container">
    <iframe width="642" height="361" src="https://www.youtube.com/embed/1pVaVyWGkpM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<br>
Nous nous allons faire une <b>animation simple</b> qui va correspondre à <b>sa respiration</b>. Et oui, notre bombe respire ^^.

C'est très simple à réaliser, il suffit par exemple de descendre le corps de la bombe ou de racourcir les pieds.

En image, ce sera plus clair :

<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/piskel_animation_ennemi_bombe_idle/bombe_idle-1.png.png" alt="L'image de notre personnage, rien n'a changé" width="50%">
</center>
<br>

<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/piskel_animation_ennemi_bombe_idle/bombe_idle-2.png.png" alt="Notre personnage avec les pieds raccourci" width="50%">
</center>
<br>

<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/piskel_animation_ennemi_bombe_idle/bombe_idle.png" alt="Les deux images précédentes à la suite" width="50%">
</center>
<br>

Voici le résultat en GIF :

<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/piskel_animation_ennemi_bombe_idle/bombe_idle.gif" alt="Animation de notre personnage" width="50%">
</center>
<br>

J'espère que cet article vous a plu dans un prochain article, nous ferons marcher notre personnage.
