Title: Logo avec MagicaVoxel
Date: 2021-02-21 21:00
Tags: dessin,  pixel, irobotechart, PopArTech, MagicaVoxel
Category: iPixelArtAnim
Authors: Victor Lohézic
Summary: Nous allons créer le logo de PopArTech dans le logiciel MagicaVoxel.
Image: images/ipixelartanim/logo_popartech_magicavoxel/thumbnail.png
Type: Article
Status: published

<b>Nous allons dessiner en 3D le logo de PopArTech.</b> Pour cela, nous allons utiliser le logiciel [MagicaVoxel](https://popartech.com/magicavoxel.html). Voici des tutos pour se familiariser avec le logiciel (si besoin) :
<br>
<br>
<div class="video-container">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLYmCMg3QL20-jQNEyAAmKjGteZDxVesmy" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<br>
Cet article sera composé de petites missions.
<script src="https://aframe.io/releases/0.5.0/aframe.min.js"></script>


  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Mission 1</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Dessine une sphère :
            <center><br><img class="responsive-img" width="50%" src="images/ipixelartanim/logo_popartech_magicavoxel/sphere.png" alt="Sphère"></center>
          </span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Mission 2</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Trace le I en enlevant des voxels :
            <center><br><img class="responsive-img" width="50%" src="images/ipixelartanim/logo_popartech_magicavoxel/i.png" alt="Sphère avec I"></center>
          </span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e1bee7 purple lighten-4">
          <span class="card-title black-text"><b>Mission 3</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="black-text">Colorie le logo :
            <center><br><img class="responsive-img" width="50%" src="images/ipixelartanim/logo_popartech_magicavoxel/thumbnail.png" alt="Logo coloré"></center>
          </span>
      </div>
    </div>
  </div>


  Bravo, tu as réalisé le logo de PopArTech !
  <br>
  <br>
  <div class="video-container">
      <iframe width="560" height="315" src="https://www.youtube.com/embed/td05o14S2mY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>
  <br>
<center>
  <a class="waves-effect waves-light btn " href="https://drive.google.com/file/d/1lLRglHvOuwRxQwlqpNf6HtmF5rUwCYAi/view?usp=sharing"><i class="material-icons left">download</i>Télécharger le fichier final</a>
</center>

  <script src="https://aframe.io/releases/0.5.0/aframe.min.js"></script>
