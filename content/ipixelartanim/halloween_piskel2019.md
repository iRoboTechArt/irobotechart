Title: Halloween avec Piskel
Date: 2019-10-30 21:00
Tags: dessin,  pixel, piskel, iRoboTechArt, halloween
Category: iConceptMake3D
Authors: Victor Lohézic
Summary: Nous allons dessiner une citrouille en pixel-art pour Halloween.</b> Pour cela, nous allons utiliser le logiciel [Piskel](https://irobotechart.com/piskel-un-editeur-de-sprites.html).
Image: images/ipixelartanim/halloween_piskel/thumbnail.png
Type: Article
Status: published

<script>

  $(document).ready(function(){
    $('.collapsible').collapsible();
  });
    </script>

<b>Nous allons dessiner une citrouille en pixel-art pour Halloween.</b> Pour cela, nous allons utiliser le logiciel [Piskel](https://irobotechart.com/piskel-un-editeur-de-sprites.html). Si vous voulez vous familiariser avec ce logiciel, voici quelques articles : [Créer son smiley avec Piskel](https://irobotechart.com/creer-son-smiley-avec-piskel.html), [Piskel, création d'un personnage de jeux vidéo](https://irobotechart.com/piskel-creation-dun-personnage-de-jeux-video.html), [Piskel, animation d'un personnage](https://irobotechart.com/piskel-animation-dun-personnage.html) et [Piskel, notre personnage marche](https://irobotechart.com/piskel-notre-personnage-marche.html).
<br>
<br>
<center>
    <img class="responsive-img" src="images/ipixelartanim/halloween_piskel/thumbnail.png" width="50%" alt="Joyeux Halloween">
</center>
<br>
<br>
Cet article sera composé de petites missions.
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e65100 orange darken-4">
          <span class="card-title white-text"><b>Mission 1</b>
                        <img align="right" class="responsive-img" width="5%" src="images/ipixelartanim/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Dessinez la citrouille suivante :
            <center><br><img class="responsive-img" width="50%" src="images/ipixelartanim/halloween_piskel/mission1.png" alt="Contour de la citrouille"></center>
          </span>
      </div>
    </div>
  </div>
     <ul class="collapsible">
    <li>
      <div class="collapsible-header black white-text"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body black"><span class="white-text">
          Ce n'est pas si facile, pour obtenir la solution, il va falloir répondre à une petite question.
            <div id="zoneFormulaire">
                <label>Remplacez les pointillés par le mot manquant : Piskel permet de faire du ....... art
                    <input class="white-text" type="text" id="reponse1" />
                </label>
                <a class="waves-effect waves-light btn"  id="btnRepondre1">Répondre</a>
            </div>
          <div id="solution1">
          </div>
          <script>
              var vBtnAj = document.getElementById("btnRepondre1");
              vBtnAj.addEventListener("click", afficher);
              function afficher() {
                                var rep = document.getElementById("reponse1");
                                if(rep.value == "pixel"){
                                    document.getElementById("solution1").innerHTML = '<a href="https://drive.google.com/open?id=1igR8OPBM7hy1t8sDwTd8zo9YxxpmB5nn">Solution 1</a>';
                                }
                            }
          </script>
          </span></div>
    </li>
  </ul>
    <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e65100 orange darken-4">
          <span class="card-title white-text"><b>Mission 2</b>
                        <img align="right" class="responsive-img" width="5%" src="images/ipixelartanim/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Coloriez la citrouille :
            <center><br><img class="responsive-img" width="50%" src="images/ipixelartanim/halloween_piskel/mission2.png" alt=" La citrouille en orange"></center>
          </span>
      </div>
    </div>
  </div>
   <ul class="collapsible">
    <li>
      <div class="collapsible-header black white-text"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body black"><span class="white-text">
          <div>
                <center>
                    <iframe title="vimeo-player" src="https://player.vimeo.com/video/23896040" width="640" height="360" frameborder="0" allowfullscreen></iframe>
                </center>
          </div>
          <div id="zoneFormulaire">
                <label>Remplacez les pointillés par le mot manquant : La vidéo illustre l'Art ...
                    <input class="white-text" type="text" id="reponse2" />
                </label>
                <a class="waves-effect waves-light btn" id="btnRepondre2">Répondre</a>
            </div>
          <div id="solution2">
          </div>
          <script>
              var vBtnAj = document.getElementById("btnRepondre2");
              vBtnAj.addEventListener("click", afficher);
              function afficher() {
                                var rep = document.getElementById("reponse2");
                                if(rep.value == "ASCII"){
                                    document.getElementById("solution2").innerHTML = '<a href="https://drive.google.com/open?id=1hzZbrLT9r2TuBao-RWsyFEpKW8QPCySJ">Solution 2</a>';
                                }
                            }
          </script>
          </span></div>
    </li>
  </ul>
    <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e65100 orange darken-4">
          <span class="card-title white-text"><b>Mission 3</b>
                        <img align="right" class="responsive-img" width="5%" src="images/ipixelartanim/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Améliorez la citrouille :
            <center><br><img class="responsive-img" width="50%" src="images/ipixelartanim/halloween_piskel/mission3.png" alt="Citrouille colorée"></center>
          </span>
      </div>
    </div>
  </div>
   <ul class="collapsible">
    <li>
      <div class="collapsible-header black white-text"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body black"><span class="white-text">
          <center>
          <iframe width="727" height="409" src="https://www.youtube.com/embed/wRx22XEyZSk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </center>
          <div id="zoneFormulaire">
                <label>Quel est le materiau du filament utilisé par l'imprimante 3D ? (indice : 3 lettres)
                    <input class="white-text" type="text" id="reponse3" />
                </label>
                <a class="waves-effect waves-light btn" id="btnRepondre3">Répondre</a>
            </div>
          <div id="solution3">
          </div>
          <script>
              var vBtnAj = document.getElementById("btnRepondre3");
              vBtnAj.addEventListener("click", afficher);
              function afficher() {
                                var rep = document.getElementById("reponse3");
                                if(rep.value == "PLA"){
                                    document.getElementById("solution3").innerHTML = '<a href="https://drive.google.com/open?id=1S_SSHTS9M7yHrhSrUcwPmO0WWL-5zuVZ">Solution 3</a>';
                                }
                            }
          </script>
          </span></div>
    </li>
  </ul>
<div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e65100 orange darken-4">
          <span class="card-title white-text"><b>Mission 4</b>
                        <img align="right" class="responsive-img" width="5%" src="images/ipixelartanim/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Dessinez un personnage de Space Invaders :
            <center><br><img class="responsive-img" width="50%" src="images/ipixelartanim/halloween_piskel/mission4.png" alt="Space Invaders"></center>
          </span>
      </div>
    </div>
  </div>
   <ul class="collapsible black">
    <li>
      <div class="collapsible-header black white-text"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body white-text"><span>
          <div id="zoneFormulaire">
                <label>En quelle année est sorti le jeu <i>Space Invaders</i> ?
                    <input class="white-text" type="text" id="reponse4" />
                </label>
                <a class="waves-effect waves-light btn" id="btnRepondre4">Répondre</a>
            </div>
          <div id="solution4">
          </div>
          <script>
              var vBtnAj = document.getElementById("btnRepondre4");
              vBtnAj.addEventListener("click", afficher);
              function afficher() {
                                var rep = document.getElementById("reponse4");
                                if(rep.value == "1978"){
                                    document.getElementById("solution4").innerHTML = '<a href="https://drive.google.com/open?id=1sLDzVvNnC41PKhgT2adWrXr3ZeAwfqPR">Solution 4</a>';
                                }
                            }
          </script>
</span></div>
    </li>
  </ul>

  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e65100 orange darken-4">
          <span class="card-title white-text"><b>Mission 5</b>
                        <img align="right" class="responsive-img" width="5%" src="images/ipixelartanim/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Complétez la citrouille :
            <center><br><img class="responsive-img" width="50%" src="images/ipixelartanim/halloween_piskel/mission5.png" alt="Citrouille avec Space Invaders"></center>
          </span>
      </div>
    </div>
  </div>
   <ul class="collapsible black">
    <li>
      <div class="collapsible-header black white-text"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body white-text"><span>
                <center>
                  <iframe src="https://player.vimeo.com/video/51959225" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
              </center>
                    <div id="zoneFormulaire">
                <label>Quelle technique est utilisée dans cette vidéo ? (Mettre une majuscule au début des deux mots)
                    <input class="white-text" type="text" id="reponse5" />
                </label>
                <a class="waves-effect waves-light btn" id="btnRepondre5">Répondre</a>
            </div>
          <div id="solution5">
          </div>
          <script>
              var vBtnAj = document.getElementById("btnRepondre5");
              vBtnAj.addEventListener("click", afficher);
              function afficher() {
                                var rep = document.getElementById("reponse5");
                                if(rep.value == "Stop Motion"){
                                    document.getElementById("solution5").innerHTML = '<a href="https://drive.google.com/open?id=1TplPVrFVg-L9VLeZBQ53gjn01ihgiYWi">Solution 5</a><br><a href="https://youtu.be/Kyks2jKl0FQ">Bonus</a>';
                                }
                            }
          </script>
</span></div>
    </li>
  </ul>


  Bravo, maintenant, il ne vous reste plus qu'à exporter l'image. Et Joyeux Halloween !

<br>
<div class="video-container">
    <iframe width="727" height="409" src="https://www.youtube.com/embed/vx3Q80JY3C0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
