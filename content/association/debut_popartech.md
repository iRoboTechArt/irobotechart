Title: Et hop... PopArTech est né ! 
Date: 2023-09-12 14:12
Tags: association
Category: Association
Authors: L'équipe Com. PopArTech
Summary: L’Association iRoboTechArt change officiellement de nom et devient désormais PopArTech !
Image: images/association/debut_popartech/thumbnail.png
Type: Article
State: published


<center>
    <img src="images/association/debut_popartech/popartech.png"  class="responsive-img" alt="carte visite PopArTech">
</center>

L’Association iRoboTechArt change officiellement de nom et devient désormais PopArTech !
<br>
10 années bien remplies en particulier dans le domaine de la robotique mais désormais place à des Projets élargis touchant la PopCulture au sens large : CrepArt, Cosplay, impression3D, Mangas, PixelArt, Game, ClipVidéo, Robotique, et d’autres a venir, ... ! 
<br><br>
Notre bureau a été également renouvelé avec une équipe super dynamique !<br>
Direction : Mélodie, Aboubacar, Julian, Maxime<br>
Communication  : Aurore, Yuna, Sylvain, Max, Matteo, Macéo<br>
Gestion : Mia, Lucile, Louis, Christophe<br>

<center>
	<figure>
    	<img src="images/association/debut_popartech/irobotechart.png"  class="responsive-img" alt="carte visite iRoboTechart" width="50%">
		<figcaption><i>Carte d’identité de l’asso en 2014</i></figcaption>
	</figure>
</center>


Changement de nom de l’Association, une organisation plus structurée et lisible : l’association évolue avec son temps et les Makers qui la construisent à leurs images.
