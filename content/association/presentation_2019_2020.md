Title: Quoi de neuf pour cette année ?
Date: 2019-09-28 11:42
Tags: association, 2019, 2020, presentation, iRoboTechArt
Category: Association
Authors: Christophe d'iRTA
Summary: Cinquième nouvelle rentrée pour notre <b>Asso iRoboTechArt</b> avec cette année pas mal de nouveautés ! Tout d’abord une nouvelle organisation qui va permettre de mettre en avant les projets phares de l’Asso et cette année il y en a Sept.
Image: images/association/presentation_association_2019_2020/thumbnail.png
Type: Article
State: published


<center>
    <img src="https://irobotechart.com/theme/img/logo.png"  alt="image logo">
</center>

Cinquième nouvelle rentrée pour notre <b>Asso iRoboTechArt</b> avec cette année pas mal de nouveautés ! Tout d’abord une nouvelle organisation qui va permettre de mettre en avant les <b> projets phares de l’Asso </b> et cette année il y en a <b>Sept</b> :
<br>
<br>
1. <b>PROjet CoBotGo</b> : Construire, compétiter, explorer… de la Robotique Collaborative !
<br>
2. <b>PROjet Mangas</b> : Apprendre à dessiner, partager, lire, exposer,… univers Mangas !
<br>
3. <b>PROjet Cosplay</b> : Réaliser, exposer, explorer, jouer… votre Cosplay préféré !
<br>
4. <b>PROjet Game</b> : Concevoir, produire, compétiter … votre jeu vidéo mais pas que !
<br>
5. <b>PROjet i2D/3D</b> : Concevoir, produire, ... des objets 3D et Monter une i3D !
<br>
6. <b>PROjet CrepArt</b> : Concevoir, produire, développer, … la CrepArt !
<br>
7. <b>PROjet RetroNumérique</b> : Réparer, collectionner, simuler… machines Numériques !
<br>
<br>
<b>Nouveau planning !</b> avec désormais des créneaux d’ouverture de LABs plus importants :
<center>
	<br>1. <b>Mercredis de 13h30 à 16h30</b>
	<br>2. <b>Samedis 10h à 13h30</b>
</center>
<br>
Enfin un site Web avec un contenu encore plus riche et varié avec pleins d’infos, d’actualité, des reportages, des interviews, des tutos, des vidéos... de l’Asso et de ses thématiques préférées !
<br>
<br>
Vous êtes prêts ? alors Go ! Bonne rentrée à tous et que <b>la force d’iRTA soit avec vous !</b> ;-)

Christophe
<center>
	<img src="images/association/presentation_association_2019_2020/images.PNG"  width="100%" alt="Image de crêpe art">
</center>
