Title: GeekDaysRennes2020
Date: 2020-04-12 18:45
Tags: Japon, Cosplay, Geek, Convention, Mangas, Jeux vidéo
Category: EXPLOration
Authors: Yuna G., Antonio, Maiwenn, Alexis, Paul, Max, Christophe d'iRTA
Summary: La GeekDays à Rennes se déroule du 22 au 23 Février 2020. Une jeune convention que nous sommes allés EXPLorer pour la premiere fois. Alors, est ce que cette convention tient ses promesses ? ...
Image: images/exploration/geekdays20/EtiquetteGeekDaysRennes20.png
Type: Article
Status: published

<center>
    <img class="responsive-img" src="https://stockage.irobotechart.com/wl/?id=e8x6JSsx9zeRFZTBTrA6RlcRzLl8w3N1" alt="AfficheGeekDaysRennes20">
</center>
<br>

15 jeunes de l’Asso sont allés EXPLorer pour la première fois cette toute jeune convention « La GeeDays à Rennes » le Samedi 22 Février 2020. Elle existe depuis 2018 et donc c’est sa troisième édition. Elle est déjà riche en activités avec ses Cosplays, jeux vidéo, réalité virtuelle, e-sport, univers Star Wars, Harry Potter et biensur nos Youtubers préférés il y en a pour tous les goûts ! Voila ce que nous promet cette convention en 48h top chrono !  <br><br>

-- Oui c’est une petite convention mais l’ambiance est génialement super sympas, les dessins sont magnifiques et il y en vraiment pour tous les gouts… (Maiwenn)<br><br>

-- La scène cosplay était très bonne, notamment grâce aux animateurs du concours qui avaient beaucoup d'expérience, la communauté autour du cosplay était très chaleureuse ce qui m'a donné envie d'en faire… (Antonio)<br><br>

-- C’était vraiment excellent ! Pour ma première convention. Tout m’a plu c’était vraiment génial (Alexis)<br><br>

-- J'ai personnellement aimé la GeekDays car les activités du salon étaient variées, il n'y avait pas trop de monde donc c'est respirable et on peut échanger tranquille avec d'autres et puis il y avait des gens "connu" tels que des youtubeurs (Frigiel, Devovo, Skyart, Sweetnaillik et Farod) ou des cosplayers bien que le salon ne soit pas si célèbre que ça. Normal il est tout jeune mais il mérite le détour (Max)<br><br>

-- J'ai bien aimé rencontrer Skyyart , Frigiel, et jouer sur la scène à Rocket League, et faire des matchs FIFA. Trop cool cette convention (Paul)<br><br>

-- Une bonne petite convention bien sympa car les animations sont riches variés tant dans les jeux vidéo, Cosplays, Mangas,... Bref une convention qui mérite le détour : On y retourne l'année prochaine ! :-))  (Christophe)<br><br>

-- J’ai trouvé le salon GeekDays très intéressant. Il est beaucoup plus petit qu’un grand salon comme la Paris Games Week, certes mais il en demeure pas moins très intéressant, avec beaucoup de stands différentes, ouvrant à de nombreuses possibilités d’activités. Les stands m’ont bien plus, que ce soient ceux des artistes, ceux des jeux rétro, le coin compétition de "Mortal Kombat" ou encore le coin karaoké. J’ai passé un agréable moment, et j’ai hâte d’y retourner ! (Yuna)<br><br>
<br>
<div class="video-container">
	<iframe width="1194" height="672" src="https://www.youtube.com/embed/zCLhf4NQOsk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<br>

Maiwenn, Yuna, Antonio, Alexis, Max, Paul, Christophe
