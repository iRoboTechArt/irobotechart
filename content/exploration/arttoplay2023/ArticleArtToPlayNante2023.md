Title: Art To Play 2023 : Créativité et Compétition au Rendez-vous 
Date: 2023-11-25 17:08
Tags: Cosplay, Geek, Convention, Mangas, Jeux vidéo
Category: EXPLOration
Authors: Aboubacar M., Salomé G., Max C.
Summary: Plongez dans l'univers fantastique de la créativité, où l'art et le jeu fusionnent pour offrir une expérience inoubliable : bienvenue à <strong>Art To Play 2023</strong> !  Pour sa 12e édition à Nantes sur le Week-end du 18 au 19 Novembre 2023 ...
Image: images/exploration/arttoplay2023/atp2023.jpg
Type: Article
Status: published

<center>
    <img class="responsive-img" src="images\exploration\arttoplay2023\logoArtToPlayNoir.jpg" alt="Logo de la Art To Play">
</center>
<br><br>

Plongez dans l'univers fantastique de la créativité, où l'art et le jeu fusionnent pour offrir une expérience inoubliable : bienvenue à <strong>Art To Play 2023</strong> !  Pour sa 12e édition à Nantes sur le Week-end du 18 au 19 Novembre 2023.<br>
Comme chaque année, on y retrouve une multitude de stands tous proposant de vous faire découvrir ou redécouvrir de nouveaux horizons à travers les activités ou présentations qu’ils proposent.<br>
Nous ne sommes pas en reste avec nos 2 stands <strong>CrépArt</strong> et <strong>Créagames</strong> qui n’ont rien à envier aux autres stands XD.<br><br>

<strong>Le stand CréaGames :</strong><br>
Nous étions là (au stand 208 dans le Hall XXL 😉) avec notre équipe de Games Maker avec au menu des créations qui s’appuie principalement sur Minecraft dont les thèmes étaient :
Du parcours, du PvP (joueur contre joueur), de la réflexion et des tournois avec une médaille à la clé.
On n'en attendait pas moins de <strong>PopArTech</strong>.<br><br>

<style>
        /* Conteneur du carrousel  */
        .carousel-container1 {
            width: 173vw;
            overflow: hidden; /* Cache tout ce qui dépasse du cadre */
            position: relative;
        }

        /* Bande de défilement des images */
        .carousel1 {
            display: flex;
            justify-content: flex-start;
            align-items: center;
            animation: scroll 32s linear infinite; /* Défilement fluide et continu */
        }

        /* Style pour chaque image */
        .carousel1 img {
            margin-right: 1vw;
            width: auto;
            height: 20vw; /* Ajuste la hauteur des images */
            flex-shrink: 0; /* Empêche les images de se réduire */
        }

        /* Animation pour le défilement en continu */
        @keyframes scroll {
            0% { transform: translateX(0); }
            45% { transform: translateX(-74%); } /* Petite pause */
            50% { transform: translateX(-74%); }/* Demi tour contenu */
            95%{ transform: translateX(0)}     /* Petite pause */
            100%{ transform: translateX(0)}
        }

        /* Groupes d'images côte à côte */
        .group1 {
            display: flex;
        }

        .group1 img {
            margin-right: 0.5vw; /* Espace entre les images d'un même groupe */
        }
</style>

<div class="carousel-container1">
    <div class="carousel1">
        <!-- Groupe d'images 1 -->
        <div class="group1">
            <img src="images\exploration\arttoplay2023\void1.png" alt="Vue de loin de la map Voidland">
            <img src="images\exploration\arttoplay2023\void2.png" alt="Vue de haut de la map Voidland">
            <img src="images\exploration\arttoplay2023\void3.png" alt="Vue approffondie de la map voidland">
        </div>
        <!-- Groupe d'images 2 -->
        <div class="group1">
            <img src="images\exploration\arttoplay2023\ctf1.png" alt="Image du drapeauw de l'équipe bleu">
            <img src="images\exploration\arttoplay2023\ctf2.png" alt="Image du pont suspendu séparent les deux équipes">
            <img src="images\exploration\arttoplay2023\ctf3.png" alt="Image d'une partie de la map du capture de drapeaux">
        </div>
    </div>
</div><br><br>

Vous avez compris, on envoie du lourd, du très lourd jusqu’à devenir une force d’attraction qui a attiré une foule de personnes à notre stand.<br>
Et le clou du spectacle de ce stand, le tournoi PvP sur la carte <strong>VOIDLAND</strong>, dont le top 3 qui a su montrer une stratégie des plus astucieuse s'est vu octroyer une gagnante.<br><br>

<style>
        /* Conteneur principal pour les images */
        .doubleImg {
            display: flex; /* Affiche les éléments enfants en ligne */
            justify-content: center; /* Centre le contenu horizontalement */
            align-items: center; /* Aligne verticalement */
        }

        /* Conteneur pour les images */
        .image-container {
            display: flex; /* Permet d'aligner les images côte à côte */
            flex-wrap: wrap; /* Permet le passage à la ligne si nécessaire */
            gap: 0.5vw; /* Ajoute de l'espace entre les images */
            justify-content: center; /* Centre le contenu à l'intérieur */
            width: 100%; /* Utilise toute la largeur disponible */
        }

        /* Rendre les images responsives */
        .responsive-img1 {
            max-width: 57%; /* Chaque image occupe jusqu'à 45% de la largeur */
            height: auto; /* Conserve les proportions */
            object-fit: contain; /* Adapte l'image pour qu'elle soit visible entièrement */
            display: block; /* Supprime les espaces indésirables */
        }
        .responsive-img2 {
            max-width: 33%; /* Chaque image occupe jusqu'à 45% de la largeur */
            height: auto; /* Conserve les proportions */
            object-fit: contain; /* Adapte l'image pour qu'elle soit visible entièrement */
            display: block; /* Supprime les espaces indésirables */
        }
</style>

<div class="doubleImg">
    <div class="image-container">
        <img class="responsive-img1" src="images/exploration/arttoplay2023/creagame-top3.png" 
             alt="Top 3 du tournois PvP organisé par PopArtech">
        <img class="responsive-img2" src="images/exploration/arttoplay2023/creagame-top1.png" 
             alt="Top 1 du tournois PvP organisé par PopArtech">
    </div>
</div>
<br><br>

Dans ce tournoi réunissant 8 joueurs, l’objectif était simple : survivre et récolter le plus de gemmes. Les joueurs pouvaient obtenir des gemmes soit en les ramassant sur les différentes plateformes suspendues dans le vide, soit en éliminant leurs adversaires pour ralentir leur progression et assurer leur propre victoire. Chaque joueur disposait de 3 vies, et la moindre erreur "comme tomber dans le vide" pouvait s’avérer fatale. À la fin du temps imparti, le joueur avec le plus de gemmes remportait la partie.
Avec les créateurs de cette map Gabriel, Esteban et Ethan, qui sans eux <strong>VOIDLAND</strong> n’aurait jamais vu le jour.
<br><br>

Mais le plaisir ne s'arrête pas là ! En plus des épreuves épiques de <strong>VOIDLAND</strong>, nos visiteurs ont également eu l'occasion de laisser libre cours à leur créativité sur le stand <strong>CrépArt</strong>. 
<br><br>

<strong>Le stand CrépArt :</strong><br>
Toujours plus populaire au fil des conventions, peu de visiteurs à Art To Play ont pu résister à l'attrait du stand CrépArt. De la conception sur papier à la reproduction sur bilique, le CrépArt a conquis les foules grâce à sa combinaison de créativité et de gourmandise. Même <strong>Brunograffer</strong> a pris part à l’activité, attirant encore plus de curieux.<br><br>

<div class="image-container">
    <img class="responsive-img" src="images\exploration\arttoplay2023\crepart-Artiste.png" alt="Brunograffer fesant du crepart">
</div>

<style>
        /* Conteneur du carrousel */
        .carousel-container {
            width: 275vw;
            overflow: hidden; /* Cache tout ce qui dépasse du cadre */
            position: relative;
        }

        /* Bande de défilement des images */
        .carousel {
            display: flex;
            justify-content: flex-start;
            align-items: center;
            animation: scroll 64s linear infinite; /* Défilement fluide et continu */
        }

        /* Style pour chaque image */
        .carousel img {
            margin-right: 1vw;
            width: auto;
            height: 20vw; /* Ajuste la hauteur des images */
            flex-shrink: 0; /* Empêche les images de se réduire */
        }

        /* Animation pour le défilement en continu */
        @keyframes scroll {
            0% { transform: translateX(0); }
            45% { transform: translateX(-74%); } /* Petite pause */
            50% { transform: translateX(-74%); }/* Demi tour contenu */
            95%{ transform: translateX(0)}     /* Petite pause */
            100%{ transform: translateX(0)}
        }

        /* Groupes d'images côte à côte */
        .group {
            display: flex;
        }

        .group img {
            margin-right: 0.5vw; /* Espace entre les images d'un même groupe */
        }
</style>

<!-- Conteneur pour le carrousel -->
<div class="carousel-container">
    <div class="carousel">
        <!-- Groupe d'images 1 -->
        <div class="group">
            <img src="images\exploration\arttoplay2023\crepart-1.png" alt="Image d'une personne tenant son dessin et son crepart">
            <img src="images\exploration\arttoplay2023\crepart-2.png" alt="Image d'une personne tenant son dessin et son crepart">
            <img src="images\exploration\arttoplay2023\crepart-3.png" alt="Image d'une personne tenant son dessin et son crepart">
            <img src="images\exploration\arttoplay2023\crepart-4.png" alt="Image d'une personne tenant son dessin et son crepart">
        </div>
        <!-- Image seule -->
        <img src="images\exploration\arttoplay2023\crepart-5.png" alt="Image de la réalisation d'un crepart sur bilique et en bonus une imageception">
        <!-- Groupe d'images 2 -->
        <div class="group">
            <img src="images\exploration\arttoplay2023\crepart-7.png" alt="Image d'une personne tenant son dessin et son crepart">
            <img src="images\exploration\arttoplay2023\crepart-8.png" alt="Image de la réalisation d'un crepart sur bilique">
            <img src="images\exploration\arttoplay2023\crepart-9.png" alt="Image d'une personne tenant son dessin et son crepart">
            <img src="images\exploration\arttoplay2023\crepart-10.png" alt="Image d'une personne tenant son dessin et son crepart">
        </div>
        <!-- Image seule -->
        <img src="images\exploration\arttoplay2023\crepart-6.png" alt="Image de la réalisation d'un crepart sur bilique">
        <!-- Groupe d'images 3 -->
        <div class="group">
            <img class="responsive-img" src="images\exploration\arttoplay2023\crepart-pancarte1.png" alt="Pancarte explicatif du stand Crepart">
            <img class="responsive-img" src="images\exploration\arttoplay2023\crepart-pancarte2.png" alt="Pancarte explicatif du stand Creparth">
            <img class="responsive-img" src="images\exploration\arttoplay2023\crepart-pancarte3.png" alt="Pancarte explicatif du stand Crepart">
            <img class="responsive-img" src="images\exploration\arttoplay2023\crepart-pancarte4.png" alt="Pancarte explicatif du stand Crepart">
        </div>
    </div>
</div>

<!-- <div class="image-container">
    <img class="responsive-img" src="images\exploration\arttoplay2023\crepart-pancarte0.png" alt="Toutes les pancarte explicatif du stand Crepart">
</div> --> 
<br>


Sur le stand, les participants étaient guidés par des membres de l’association pour rendre leur expérience aussi agréable que possible. Ils commençaient par dessiner leur motif ou personnage préféré sur papier, puis le reproduisaient sur une machine à crêpes, à l'aide de tubes qui déposaient progressivement de la pâte à crêpe. Trois teintes de couleur, allant du beige au marron foncé, étaient disponibles pour ajouter des nuances et donner vie à leurs créations. Une fois leur crêpe artistique terminée, les visiteurs pouvaient la déguster sur place. Beaucoup ont aussi choisi de laisser leur dessin pour contribuer à une grande fresque exposée tout le week-end.<br><br>

Ne ratez pas la prochaine édition d'<strong>Art To Play</strong>, où nous vous proposerons encore plus de défis créatifs et compétitifs avec <strong>CréaGames</strong> et des moments gourmands avec <strong>CrépArt</strong> ! Rejoignez-nous pour vivre des expériences uniques et participer à des activités ludiques où créativité et amusement se rencontrent.<br>
Vous souhaitez nous soutenir davantage ? Vous pouvez également contribuer à l’aventure en faisant un don à notre association. Chaque contribution nous permet de continuer à organiser ces événements et de proposer des stands toujours plus innovants. Que ce soit pour financer du matériel, développer de nouvelles idées, ou simplement nous soutenir, chaque <strong>don</strong> fait la différence !<br><br>

Nous espérons vous retrouver encore plus nombreux à <strong>Art To Play 2024</strong> pour de nouvelles aventures créatives et des expériences uniques ! Venez partager avec nous un moment inoubliable l’année prochaine, à très vite !<br><br>


Aboubacar M., Salomé G., Max C.
