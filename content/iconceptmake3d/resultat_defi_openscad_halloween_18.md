Title: Logo 
Date: 2018-11-04 21:38
Tags: iConceptMake3D, openscad
Category: iConceptMake3D
Authors: Victor Lohézic
Summary: Venez découvrir les résultats et les réalisations du Défi OpenSCAD Halloween 18.
Image: images/iconceptmake3D/resultat_defi_openscad_halloween_18/thumbnail.jpg
Type: Article
Status: published

Suite à l'article [Halloween avec OpenScad](halloween-avec-openscad.html), un défi avec le logiciel OpenSCAD a été lancé. Ce concours terminé, nous allons vous présenter leur création. Pour mieux comprendre comment ils ont réalisé leur pièce, nous vous proposons deux images. La première est issue du logiciel permettant de créer une police et la seconde du logiciel OpenSCAD.
<br>
<br>
Tout d'abord, Yuna a modélisé un <b>Toad Chauve-Souris</b> :

<br>
<center>
    <img class="responsive-img" src="images/iconceptmake3D/resultat_defi_openscad_halloween_18/toad.jpg" alt="Toad Chauve-Souris dans le logiciel de création de police/toad.jpg" width="50%">
</center>
<center>
    <img class="responsive-img" src="images/iconceptmake3D/resultat_defi_openscad_halloween_18/toad_openscad.jpg" alt="Toad Chauve-Souris dans OpenSCAD" width="50%">
</center>
<br>
<br>
Ensuite, Thao a travaillé sur une <b>araignée</b> :
<br>
<center>
    <img class="responsive-img" src="images/iconceptmake3D/resultat_defi_openscad_halloween_18/araignee.jpg" alt="Araignée dans le logiciel de création de police" width="50%">
</center>
<center>
    <img class="responsive-img" src="images/iconceptmake3D/resultat_defi_openscad_halloween_18/araignee_openscad.jpg" alt="Araignée dans OpenSCAD" width="50%">
</center>
<br>
<br>
Puis, Tim a imaginé un <b>crâne de squelette</b> :
<br>
<center>
    <img class="responsive-img" src="images/iconceptmake3D/resultat_defi_openscad_halloween_18/tete_de_mort.jpg" alt="Tête de mort dans le logiciel de création de police" width="50%">
</center>
<center>
    <img class="responsive-img" src="images/iconceptmake3D/resultat_defi_openscad_halloween_18/tete_de_mort_openscad.jpg" alt="Tête de mort dans OpenSCAD" width="50%">
</center>
<br>
<br>
Et enfin Christophe a dessiné une <b>citrouille</b> :
<br>
<center>
    <img class="responsive-img" src="images/iconceptmake3D/resultat_defi_openscad_halloween_18/citrouille.jpg" alt="Citrouille dans le logiciel de création de police" width="50%">
</center>
<center>
    <img class="responsive-img" src="images/iconceptmake3D/resultat_defi_openscad_halloween_18/citrouille_openscad.jpg" alt="Citrouille dans OpenSCAD" width="50%">
</center>
<br>
<br>
Voici le classement de leur création :
<br>
     <table >
        <thead>
          <tr>
              <th>Place</th>
              <th>Nom du participant</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Thao</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Yuna et Tim sont ex aequo</td>
          </tr>
          <tr>
            <td>3</td>
            <td>Christophe</td>
          </tr>
        </tbody>
      </table>

<br>
<b>Bravo à tous les participants pour leur imagination !!!</b>
