Title: OpenScad, un logiciel de modélisation 3D
Date: 2018-10-26 8:42
Tags: logiciel, CAO, 3D, imprimante 3D, halloween
Category: iConceptMake3D
Authors: Victor Lohézic
Summary: Vous voulez créér vos propres objets 3D pour ensuite les imprimer, mais vous ne savez pas par où commmencer ? Voici une présentation du logiciel OpenScad.
Image: images/iconceptmake3D/openscad/logo.png
Type: Article
Status: published

<b>Vous voulez créer vos propres objets 3D pour ensuite les imprimer, mais vous ne savez pas par où commmencer ?</b> Voici une présentation du logiciel OpenScad.
<center>
    <img class="responsive-img" src="images/iconceptmake3D/openscad/logo.png" alt="Logo OpenScad">
</center>
OpenScad est un logiciel libre sous-licence [Licence publique générale GNU](https://www.gnu.org/licenses/gpl-2.0.html). Il est aussi gratuit. Il permet la modélisation d'objets 3D. Il s'exécute sur [GNU/Linux](https://www.gnu.org/gnu/why-gnu-linux.fr.html), Windows et MAC OS X.
<br>
Les créateurs d'OpenScad lorsqu'ils ont conçu leur logiciel ont eu une autre vision que la plupart des <b>logiciels libres</b> comme [Blender](https://www.blender.org/). Ils ont préféré se concentrer sur l'aspect CAO (Conception assistée par ordinateur) que sur l'aspect artitistque. Autrement dit c'est vraiment un logiciel réfléchi pour la conception d'objets pas pour l'animation(jeux vidéos, films...).
<br>
<br>
<br>
<center>
    <img width="50%" class="responsive-img" src="images/iconceptmake3D/openscad/objet.png" alt="Exemple d'objet">
</center>
<br>
Une spécificité d'OpenScad est le fait que l'on doit écrire un <b>script</b> pour créer sa pièce. C'est très ludique d'autant plus qu'ils fournissent une feuille avec les principales fonctions(commandes)  : [http://www.openscad.org/cheatsheet/index.html](http://www.openscad.org/cheatsheet/index.html)
<br>
<center>
    <img class="responsive-img" src="images/iconceptmake3D/openscad/code.png" alt="Lignes de code">
</center>
Il possède aussi l'avantage de ne pas trop demander de ressources contrairement à d'autres logiciels de CAO. On peut facilement opter pour cette solution dans le cadre d'un projet car sa prise en main est aisée.
<br>
<br>
N'hésitez pas à venir consulter leur [site](http://www.openscad.org/index.html), [le télécharger](http://www.openscad.org/downloads.html), ou même faire un [don](http://www.openscad.org/community.html).
