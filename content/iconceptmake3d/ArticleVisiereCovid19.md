Title: Covid3D
Date: 2020-04-16 18:45
Tags: Solidarité, changer le monde, innovation, santé
Category: iConceptMake3D
Authors: Christophe d'iRTA
Summary: Le covid 19 est parmis nous ! l'Asso iRTA met le turbo pour venir en aide au monde médical mais pas que ...
Image: images/iconceptmake3D/covid3d/EtiquetteArticle.png
Type: Article
Status: published


<center>
    <img class="responsive-img" src="https://stockage.irobotechart.com/wl/?id=vIt7dzUgFshMLXr4VRM5tr33HeETkMsE" alt="EtiquetteArticle">
</center>
<br>

L’Association iRoboTechArt (7 LAB’s dont FABLAB) à Lorient [https://www.iRoboTechArt.com](https://www.iRoboTechArt.com) ce joint au mouvement d’ampleur de tous les Makers Régionaux et Nationaux afin d’aider le monde médical à combattre le Covid19 et en particulier avec :
<br>
-- Le collectif "i3D du Pays de Lorient", collectif à but non-lucratif rassemblant des acteurs de la santé et de la fabrication numérique du territoire de Lorient : CMRRF Kerpape-Ploemeur ComposiTIC/IRMA-Ploemeur, IRDL/UBS-Lorient, CREPP-Ploemeur, Breizh3D56-Ploemeur
<br>
-- Le collectif national Covid3D : [https://www.covid3d.fr](https://www.covid3d.fr) initié entre autres avec l'excellente Youtubeuse Heliox.
<br><br>
Parallèlement l’Association iRTA est en lien très étroit avec des professionnels de l’Hôpital du Scorff de Lorient et ainsi produit et offre des visières de protection. Découvrir et comprendre le fonctionnement d’un Hôpital et les métiers qui s’y trouve, c’est un plus et c’est top !
<br><br>
Donc concrètement on ne produit pas un objet 3D pour le monde médical comme ça : le cahier des charges est très serré ! Chaque pays a ses propres normes. C'est pourquoi on a suivi celui proposé par Erik Cederberg (merci !) [https://3dverkstan.se/protective-visor/protective-visor-versions/](https://3dverkstan.se/protective-visor/protective-visor-versions/) qui est simple et fonctionnel à produire, à utiliser et qui répond aux normes en vigueur :

<br>
<center>
    <img class="responsive-img" src="https://stockage.irobotechart.com/wl/?id=3M6h7bYayUM51fIheawbsYJc2mayAzhZ" alt="PhotoVisiere">
</center>
<br>


<div class="video-container">
	<iframe width="1194" height="672" src="https://www.youtube.com/embed/tj63EkS1YR4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<br>
Avantage certain de cette visière : elle n'a pas besoin d'élastique pour tenir ! Elle peut également être utilisée dans d'autres secteurs d'activités : alimentaire, la distribution, etc.
Si vous êtes intéressés merci de nous contacter par les plateformes notées ci-dessus ou tout simplement par courriel : contact@iRoboTechArt.com
<br><br>
Bon confinement à tous ...<br>
Christophe
<br>
