Title: Halloween avec OpenScad
Date: 2018-10-29 8:51
Tags: logiciel, CAO, 3D, imprimante 3D, iRoboTechArt
Category: iConceptMake3D
Authors: Victor Lohézic
Summary: Nous allons modéliser en 3D avec OpenScad la décoration pour votre soirée d'Halloween.
Image: images/iconceptmake3D/halloween_openscad/thumbnail.png
Type: Article
Status: published

<script>

  $(document).ready(function(){
    $('.collapsible').collapsible();
  });
    </script>

<b>Nous allons modéliser en 3D avec OpenScad la décoration pour votre soirée d'Halloween.</b> OpenScad permet l'intégration de texte. L'astuce que nous allons utiliser ici est de choisir une police avec des dessins d'Halloween. Si vous ne connaissez pas le logiciel OpenScad, allez voir cet [article](openscad-un-logiciel-de-modelisation-3d.html).
<br>
<br>
<center>
    <img class="responsive-img" src="images/iconceptmake3D/halloween_openscad/thumbnail.png" width="50%" alt="Joyeux Halloween">
</center>
<br>
<br>
Cet article sera composé de petites missions. Nous en ferons certaines ensemble mais pour les autres essayez d'y répondre sans regarder les solutions.
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e65100 orange darken-4">
          <span class="card-title white-text"><b>Mission 1</b>
                        <img align="right" class="responsive-img" width="5%" src="images/iconceptmake3D/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Nous allons télécharger une police avec des dessins d'Halloween. Pour cela, nous nous rendons sur [https://www.dafont.com/fr/](https://www.dafont.com/fr/). Vous pouvez ensuite rechercher Halloween. Afin d'avoir une liberté complète d'utilisation, nous prendrons une police 100% gratuite et libre. Ce sera la suivante : [Freaky Halloween](https://www.dafont.com/fr/freaky-halloween.font?l[]=10&l[]=1). Ensuite, installez la police. Sur Windows cela revient à glisser le fichier FREAKY HALLOWEEN.otf dans le dossier Fonts (situé souvent sur le disque C).
            <ul>
            <li></li>
            <li></li>
            <li></li>
            </ul>
        </span>
      </div>
    </div>
  </div>
    <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e65100 orange darken-4">
          <span class="card-title white-text"><b>Mission 2</b>
                        <img align="right" class="responsive-img" width="5%" src="images/iconceptmake3D/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Dessinez la citrouille suivante :
            <center><br><img class="responsive-img" width="50%" src="images/iconceptmake3D/halloween_openscad/citrouille.png" alt="Image de la citrouille en 3D"></center>
            Pour vous aider, je vous ai écrit en partie le code :

                content = "?\"";
                font = "FREAKY HALLOWEEN";

                linear_extrude(height = 3) {
                       text(content, font = font, spacing= 1.2, size = 10);
                }
</span>
        <b>La variable Font</b> indique la police que nous avons choisie. Le code qui suit permet de mettre en relief le texte. N'hésitez pas à modifier les valeurs pour mieux comprendre le fonctionnement. Enfin <b>la variable content</b> correspond au texte. Votre mission maintenant est de remplacer le point d'interrogation afin de modéliser une citrouille.
      </div>
    </div>
  </div>
   <ul class="collapsible">
    <li>
      <div class="collapsible-header black white-text"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body black"><span class="white-text">

                            content = "E\"";
                            font = "FREAKY HALLOWEEN";

                            linear_extrude(height = 3) {
                                   text(content, font = font, spacing= 1.2, size = 10);
                            }
<br>
          <br>
          Pour trouver <b>la lettre E</b>, il suffisait de regarder le tableau disponible [ici](https://www.dafont.com/fr/freaky-halloween.font?l[]=10&l[]=1)
          <center><br><img class="responsive-img" width="80%" src="images/iconceptmake3D/halloween_openscad/tableau_caractere.png" alt="Image du tableau"></center>
          </span></div>
    </li>
  </ul>
    <div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e65100 orange darken-4">
          <span class="card-title white-text"><b>Mission 3</b>
                        <img align="right" class="responsive-img" width="5%" src="images/iconceptmake3D/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Dessinez la sorcière suivante :
            <center><br><img class="responsive-img" width="50%" src="images/iconceptmake3D/halloween_openscad/sorciere.png" alt="Image d'une sorcière en 3D"></center>
            Plus d'indice maintenant ;)
        </span>
      </div>
    </div>
  </div>
   <ul class="collapsible">
    <li>
      <div class="collapsible-header black white-text"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body black"><span class="white-text">

                content = "A\"";
                font = "FREAKY HALLOWEEN";

                linear_extrude(height = 3) {
                       text(content, font = font, spacing= 1.2, size = 10);
                }
</span></div>
    </li>
  </ul>
<div class="row">
    <div class="col s12 m12">
      <div class="card-panel #e65100 orange darken-4">
          <span class="card-title white-text"><b>Mission 4</b>
                        <img align="right" class="responsive-img" width="5%" src="images/iconceptmake3D/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Dessiner :
            <center><br><img class="responsive-img" width="50%" src="images/iconceptmake3D/halloween_openscad/happy_halloween.png" alt="Image happy halloween en 3D"></center>
        </span>
      </div>
    </div>
  </div>
   <ul class="collapsible black">
    <li>
      <div class="collapsible-header black white-text"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body white-text"><span>

                content = "U\"";
                font = "FREAKY HALLOWEEN";

                linear_extrude(height = 3) {
                       text(content, font = font, spacing= 1.2, size = 10);
                }
</span></div>
    </li>
  </ul>

  <ul class="collapsible black">
    <li>
      <div class="collapsible-header black white-text"><i class="material-icons">computer</i>Défi OpenSCAD Halloween 18</div>
      <div class="collapsible-body white-text"><span>
          Afin d'approfondir le sujet, voici un petit défi :
          <ol>
          <li>Dessiner Halloween en 3D avec OpenSCAD et utiliser une police existente 100% gratuite et libre</li>
          <li>Dessiner Halloween en 3D avec OpenSCAD en créant son propore dessin et donc sa propre police.</li>
          </ol>
          <br>
          Voici quelques outils pour créer votre propre police :
          <ol>
            <li>[www.pentacom.jp/pentacom/bitfontmaker2/](www.pentacom.jp/pentacom/bitfontmaker2/)
            <br>gratuit en ligne, sans inscription, facile d'utilisation, fonctions simples</li>
            <li>[https://fontstruct.com/](https://fontstruct.com/)<br>
            gratuit en ligne, avec inscription, facile d'utilisation, fonctions riches</li>
        </ol>
</span></div>
    </li>
  </ul>

  Bravo, maintenant, il ne vous reste plus qu'à exporter dans le format STL et à imprimer. Et Joyeux Halloween !


<div class="video-container">
    <iframe width="547" height="410" src="https://www.youtube.com/embed/a-qFun2drLA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
