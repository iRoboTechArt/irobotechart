Title: JapanExpo 2019 Go !
Date: 2019-07-05 13:29
Tags: Japan Expo, iRoboTechArt
Category: iLiveMangasFree
Authors: Christophe d'iRTA
Summary: La Japan Expo est le rendez-vous des amoureux du Japon et de sa culture, du manga aux arts martiaux, du jeu vidéo au folklore nippon, de la J-music à la musique traditionnelle : un événementincontournable pour tous ceux qui s’intéressent à la culture japonaise et une infinité de découvertes !
Image: images/ilivemangasfree/japanexpo2019/thumbnail.png
Type: Article
Status: published


La <b>Japan Expo</b> est le rendez-vous des amoureux du Japon et de sa culture, du manga aux arts martiaux, du jeu vidéo au folklore nippon, de la J-music à la musique traditionnelle : un événement <b>incontournable</b> pour tous ceux qui s’intéressent à la culture japonaise et une infinité de découvertes ! L’Association iRoboTechArt y est présente pour être baigné dans cette convention qui réuni tous <b>les passionnées de culture pop Japan !</b>
<div class="row">
    <div class="col s12 m4 l8">
        A ne rater sous aucun prétexte d’autant plus que c’est la 20eme année d’existence... Deux journée de folles <b>EXPLOrations</b>, c’est un peu sportif mais ca vaut le coup et plein d’Actu a lire, d’ici peu, sur le site de l’Asso !
    <br>
    <br>
    L’équipe <b>iRTA-EXPLOJapanExpo2019</b><br>
    Saoirse, Lili, Derwell, Yuna, Paul, Ewen, Jeremy, Ethan,
    Perig, Achille, Martin, Tim, Thao, JeanYves et Christophe.
    </div>
    <br>
    <div class="col s12 m4 l4">
        <center>
            <img class="responsive-img" src="images/ilivemangasfree/japanexpo2019/JapanExpo19b.png" alt="JapanExpo2019">
        </center>
    </div>
</div>
<center>
    <img class="responsive-img" src="images/ilivemangasfree/japanexpo2019/JapanExpo.PNG" alt="JapanExpo2019">
</center>
