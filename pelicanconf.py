#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Victor'
SITENAME = 'PopArTech'
SITEURL = ''

#Thème
THEME = 'irobotechart'

OUTPUT_PATH = 'public/'

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_DOMAIN = SITEURL

FEED_RSS = None
FEED_ATOM = None
FEED_ALL_ATOM = 'feeds/all.atom.xml'
FEED_ALL_RSS = 'feeds/all.rss.xml'

TRANSLATION_FEED_ATOM = None
TRANSLATION_FEED_RSS = None

RSS_FEED_SUMMARY_ONLY = True

DEFAULT_PAGINATION = 15

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True



# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

TEMPLATE_PAGES  =  { 'contact.html' :  'contact.html',
                     'informations.html' :  'informations.html',
                     'calendrier.html' :  'calendrier.html',
                     'labs/designlab.html' :  'labs/designlab.html',
                     'labs/educlab.html' :  'labs/educlab.html',
                     'labs/explolab.html' :  'labs/explolab.html',
                     'labs/fablab.html' :  'labs/fablab.html',
                     'labs/livinglab.html' :  'labs/livinglab.html',
                     'labs/medialab.html' :  'labs/medialab.html',
                     'live.html' :  'live.html',
                     'equipe_irobotechart.html' :  'equipe_irobotechart.html',
                     'defis/crepart.html' : 'defis/crepart.html',
                     'defis/articles.html' : 'defis/articles.html',
                     'defis/minecraft.html' : 'defis/minecraft.html',
                     'defis/maker.html' : 'defis/maker.html',
                     'defis/paintball2.html' : 'defis/paintball2.html',
                     'defis/pixelart.html' : 'defis/pixelart.html',
                     'defis/sumobot.html' : 'defis/sumobot.html',
                     'projects/iMakeGame.html' : 'projects/iMakeGame.html',
                     'projects/iRoboTechArt.html' : 'projects/iRoboTechArt.html',
                     'defis/pixelart1/all_px1.html' : 'defis/pixelart1/all_px1.html'
                   } 

PLUGIN_PATHS = ["plugins"]

PLUGINS = ["sitemap","similar_posts"]

SITEMAP = {
    'format': 'xml'
}

SIMILAR_POSTS_MAX_COUNT = 2

DEFAULT_METADATA = {
    'type': 'Article',
    'date': '2019-03-27 16:31',
}
