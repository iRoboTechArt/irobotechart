# **PopArTech**

Bienvenue sur le dépôt officiel du [site web](https://popartech.com/) de l'association PopArTech. 

Tu ne connais pas cette association ?
Je t'invite à en découvrir plus [ici](https://popartech.com/informations.html).

Technologies utilisées :
* [Pelican](https://blog.getpelican.com/) : générateur de sites statiques
* [Materialize](https://materializecss.com/) : Framework CSS

Tutos :
* [pour écrire des articles](https://github.com/luong-komorebi/Markdown-Tutorial/blob/master/README_fr.md)
* [pour utiliser Pelican](https://zestedesavoir.com/tutoriels/2497/creer-un-site-web-statique-avec-pelican/)